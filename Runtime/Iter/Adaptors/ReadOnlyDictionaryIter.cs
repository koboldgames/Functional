namespace Koboldgames.Iter
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using Option;

    /// <summary>
    /// An iterator that adapts iteration over an IReadOnlyDictionary
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="U"></typeparam>
    public struct ReadOnlyDictionaryIter<T, U> : IExactSizeIterator<(T, U)>
    {
        private T[] keys;
        private IReadOnlyDictionary<T, U> source;
        private int length;
        private int index;

        /// <summary>
        /// Create an iterator over an IReadOnlyDictionary
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        internal static ReadOnlyDictionaryIter<T, U> New(IReadOnlyDictionary<T, U> source)
        {
            ReadOnlyDictionaryIter<T, U> v;
            v.source = source;
            v.keys = source.Keys.ToArray();
            v.length = source.Count;
            v.index = 0;
            return v;
        }

        /// <inheritdoc />
        public int Length() => length;

        /// <inheritdoc />
        public bool IsEmpty() => length == 0;

        /// <inheritdoc />
        public Option<(T, U)> Next()
        {
            if (index >= length)
            {
                return Option<(T, U)>.None;
            }

            T key = keys[index];
            U val = source[key];
            index += 1;
            return Option<(T, U)>.Some((key, val));
        }

        /// <inheritdoc />
        public (int, Option<int>) SizeHint()
        {
            return (0, Option<int>.Some(length));
        }
    }
}
