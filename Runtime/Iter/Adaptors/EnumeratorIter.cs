namespace Koboldgames.Iter
{
    using System.Collections.Generic;
    using Option;

    /// <summary>
    /// An iterator that iterates over an <c>IEnumerator</c>. This is an adapter to the C# iterator API.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public struct EnumeratorIter<T> : IIterator<T>
    {
        private IEnumerator<T> enumerator;

        /// <summary>
        /// Create an iterator over an IEnumerator
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        internal static EnumeratorIter<T> New(IEnumerator<T> source)
        {
            EnumeratorIter<T> v;
            v.enumerator = source;
            return v;
        }

        /// <inheritdoc />
        public Option<T> Next()
        {
            bool status = enumerator.MoveNext();
            if (!status)
            {
                return Option<T>.None;
            }

            T next = enumerator.Current;
            return Option<T>.Some(next);
        }

        /// <inheritdoc />
        public (int, Option<int>) SizeHint()
        {
            return (0, Option<int>.None);
        }
    }
}
