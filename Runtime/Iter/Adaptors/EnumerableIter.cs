namespace Koboldgames.Iter
{
    using System.Collections.Generic;
    using Option;

    /// <summary>
    /// An iterator that iterates over an <c>IEnumerable</c>. This is an adapter to the C# iterator API.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public struct EnumerableIter<T> : IIterator<T>
    {
        private IEnumerator<T> enumerator;

        /// <summary>
        /// Create an iterator over an IEnumerable
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        internal static EnumerableIter<T> New(IEnumerable<T> source)
        {
            EnumerableIter<T> v;
            v.enumerator = source.GetEnumerator();
            return v;
        }

        /// <inheritdoc />
        public Option<T> Next()
        {
            bool status = enumerator.MoveNext();
            if (!status)
            {
                return Option<T>.None;
            }

            T next = enumerator.Current;
            return Option<T>.Some(next);
        }

        /// <inheritdoc />
        public (int, Option<int>) SizeHint()
        {
            return (0, Option<int>.None);
        }
    }
}
