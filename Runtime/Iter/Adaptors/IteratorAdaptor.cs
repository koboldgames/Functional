using System;
using System.Collections;
using System.Collections.Generic;
using Koboldgames.Option;

namespace Koboldgames.Iter
{
    /// <summary>
    /// Adapts an iterator to the C# iterator API.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public struct IteratorAdaptor<T> : IEnumerable<T>, IEnumerator<T>
    {
        private IIterator<T> source;
        private T current;

        /// <summary>
        /// Adapts an iterator to the C# iterator API (the one based on IEnumerable and IEnumerator)
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        internal static IteratorAdaptor<T> New(IIterator<T> source)
        {
            IteratorAdaptor<T> v;
            v.source = source;
            v.current = default;
            return v;
        }

        /// <inheritdoc />
        public IEnumerator<T> GetEnumerator()
        {
            return this;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <inheritdoc />
        public bool MoveNext()
        {
            Option<T> next = source.Next();
            if (!next.isSome)
            {
                return false;
            }

            current = next.some;
            return true;
        }

        /// <inheritdoc />
        public void Reset() => throw new NotSupportedException();

        /// <inheritdoc />
        public T Current => current;

        object IEnumerator.Current => Current;

        /// <inheritdoc />
        public void Dispose()
        {
        }
    }
}
