namespace Koboldgames.Iter
{
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using Option;

    /// <summary>
    /// An iterator that adapts iteration over an IReadOnlyList.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public struct ReadOnlyListIter<T> : IExactSizeIterator<T>
    {
        private IReadOnlyList<T> source;
        private int length;
        private int index;

        /// <summary>
        /// Create an iterator over an IReadOnlyList
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        internal static ReadOnlyListIter<T> New(IReadOnlyList<T> source)
        {
            ReadOnlyListIter<T> v;
            v.source = source;
            v.length = source.Count;
            v.index = 0;
            return v;
        }

        /// <inheritdoc />
        public int Length() => length;

        /// <inheritdoc />
        public bool IsEmpty() => length == 0;

        /// <inheritdoc />
        public Option<T> Next()
        {
            if (index >= length)
            {
                return Option<T>.None;
            }

            T next = source[index];
            index += 1;
            return Option<T>.Some(next);
        }

        /// <inheritdoc />
        public (int, Option<int>) SizeHint()
        {
            return (0, Option<int>.Some(length));
        }
    }
}
