namespace Koboldgames.Iter
{
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using Option;

    /// <summary>
    /// An iterator that adapts iteration over a IReadOnlyCollection
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public struct ReadOnlyCollectionIter<T> : IExactSizeIterator<T>
    {
        private IEnumerator<T> enumerator;
        private int length;
        private int index;

        /// <summary>
        /// Create an iterator over an IReadOnlyCollection
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        internal static ReadOnlyCollectionIter<T> New(IReadOnlyCollection<T> source)
        {
            ReadOnlyCollectionIter<T> v;
            v.enumerator = source.GetEnumerator();
            v.length = source.Count;
            v.index = 0;
            return v;
        }

        /// <inheritdoc />
        public int Length() => length;

        /// <inheritdoc />
        public bool IsEmpty() => length == 0;

        /// <inheritdoc />
        public Option<T> Next()
        {
            if (index >= length)
            {
                return Option<T>.None;
            }

            bool status = enumerator.MoveNext();
            if (!status)
            {
                return Option<T>.None;
            }

            T next = enumerator.Current;
            index += 1;
            return Option<T>.Some(next);
        }

        /// <inheritdoc />
        public (int, Option<int>) SizeHint()
        {
            return (0, Option<int>.Some(length));
        }
    }
}
