namespace Koboldgames.Iter
{
    using System.Runtime.CompilerServices;
    using Option;

    /// <summary>
    ///     An interface for dealing with iterators.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IIterator<T>
    {
        /// <summary>
        ///     Advances the iterator and returns the next value.
        ///     Returns None when iteration is finished.
        ///     Individual iterator implementations may choose to resume iteration,
        ///     and so calling Next() again may or may not eventually start
        ///     returning Some(Item) again at some point.
        /// </summary>
        /// <returns></returns>




        Option<T> Next();

        /// <summary>
        /// <para>Returns the bounds on the remaining length of the iterator.</para>
        /// <para>Specifically, SizeHint() returns a tuple where the first element is the lower bound, and the second element is the upper bound.</para>
        /// <para>The second half of the tuple that is returned is an Option&lt;int&gt;. A None here means that either there is no known upper bound, or the upper bound is larger than int.MaxValue.</para>
        /// <para>Implementation notes</para>
        /// <para>It is not enforced that an iterator implementation yields the declared number of elements. A buggy iterator may yield less than the lower bound or more than the upper bound of elements.</para>
        /// <para>SizeHint() is primarily intended to be used for optimizations such as reserving space for the elements of the iterator, but must not be trusted to e.g., omit bounds checks in unsafe code. An incorrect implementation of SizeHint() should not lead to memory safety violations.</para>
        /// <para>That said, the implementation should provide a correct estimation, because otherwise it would be a violation of the trait's protocol.</para>
        /// <para>The default implementation returns (0, None) which is correct for any iterator.</para>
        /// </summary>
        /// <returns></returns>
        (int, Option<int>) SizeHint();
    }
}
