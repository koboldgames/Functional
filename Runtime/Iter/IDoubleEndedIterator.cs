namespace Koboldgames.Iter
{
    using Option;

    /// <summary>
    /// <para>An iterator able to yield elements from both ends.</para>
    /// <para>Something that implements DoubleEndedIterator has one extra
    /// capability over something that implements Iterator: the ability to also
    /// take Items from the back, as well as the front.</para>
    /// <para>It is important to note that both back and forth work on the same
    /// range, and do not cross: iteration is over when they meet in the
    /// middle.</para>
    /// <para>In a similar fashion to the Iterator protocol, once a
    /// DoubleEndedIterator returns None from a NextBack(), calling it again
    /// may or may not ever return Some again. Next() and NextBack() are
    /// interchangeable for this purpose.</para>
    /// </summary>
    public interface IDoubleEndedIterator<T> : IIterator<T>
    {
        /// <summary>
        /// <para>Removes and returns an element from the end of the iterator.</para>
        /// <para>Returns None when there are no more elements.</para>
        /// </summary>
        Option<T> NextBack();
    }
}
