namespace Koboldgames.Iter
{
    /// <summary>
    ///     Collects extension methods to IFusedIterator that come "for free" when you implement IFusedIterator.
    /// </summary>
    public static class FusedIteratorExtensions
    {
        /// <summary>
        ///     <para>Creates an iterator which ends after the first None.</para>
        ///     <para>
        ///         Optimised version of Fuse() for fused iterators, that is for iterators that
        ///         continue to return None after they first return None.
        ///     </para>
        /// </summary>
        /// <param name="source"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>



        public static IFusedIterator<T> Fuse<T>(this IFusedIterator<T> source)
        {
            return source;
        }
    }
}
