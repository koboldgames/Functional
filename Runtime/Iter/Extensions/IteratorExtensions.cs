using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;
using Koboldgames.Option;
using Koboldgames.Result;
using Koboldgames.Tuple;

namespace Koboldgames.Iter
{
    /// <summary>
    ///     Collects extension methods to IIterator that come "for free" when you implement IIterator.
    /// </summary>
    public static class IteratorExtensions
    {
        /// <summary>
        ///     Adapts an iterator to the C# iterator API (the one based on IEnumerable and IEnumerator)
        /// </summary>
        /// <param name="source"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IteratorAdaptor<T> Adapt<T>(this IIterator<T> source)
        {
            return IteratorAdaptor<T>.New(source);
        }

        /// <summary>
        ///     <para>Consumes the iterator, counting the number of iterations and returning it.</para>
        ///     <para>This method will call next repeatedly until None is encountered, returning the number of times it saw Some.</para>
        ///     <para>Note that next has to be called at least once even if the iterator does not have any elements.</para>
        /// </summary>
        /// <param name="source"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>



        public static int Count<T>(this IIterator<T> source)
        {
            var num = 0;

            while (source.Next().isSome)
            {
                num += 1;
            }

            return num;
        }

        /// <summary>
        ///     <para>Consumes the iterator, returning the last element.</para>
        ///     <para>
        ///         This method will evaluate the iterator until it returns None.
        ///         While doing so, it keeps track of the current element.
        ///         After None is returned, Last() will then return the last element it saw.
        ///     </para>
        /// </summary>
        /// <param name="source"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>



        public static Option<T> Last<T>(this IIterator<T> source)
        {
            var current = Option<T>.None;

            for (var next = source.Next(); next.isSome; next = source.Next())
            {
                current = next;
            }

            return current;
        }

        /// <summary>
        ///     <para>Returns the nth element of the iterator.</para>
        ///     <para>
        ///         Like most indexing operations, the count starts from zero,
        ///         so Nth(0) returns the first value, Nth(1) the second, and so on.
        ///     </para>
        ///     <para>
        ///         Note that all preceding elements, as well as the returned element,
        ///         will be consumed from the iterator. That means that the preceding
        ///         elements will be discarded, and also that calling Nth(0) multiple
        ///         times on the same iterator will return different elements.
        ///     </para>
        ///     <para>Nth() will return None if n is greater than or equal to the length of the iterator.</para>
        /// </summary>
        /// <param name="source"></param>
        /// <param name="n"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>



        public static Option<T> Nth<T>(this IIterator<T> source, int n)
        {
            if (n < 0)
            {
                return Option<T>.None;
            }

            var index = -1;
            Option<T> next;

            do
            {
                next = source.Next();
                index += 1;
            } while (next.isSome && index < n);

            return next;
        }

        /// <summary>
        ///     <para>Creates an iterator starting at the same point, but stepping by the given amount at each iteration.</para>
        ///     <para>Note 1: The first element of the iterator will always be returned, regardless of the step given.</para>
        ///     <para>
        ///         Note 2: The time at which ignored elements are pulled is not fixed. StepBy behaves like the sequence
        ///         Next(), Nth(step-1), Nth(step-1), …, but is also free to behave like the sequence AdvanceNAndReturnFirst(step),
        ///         AdvanceNAndReturnFirst(step), … Which way is used may change for some iterators for performance reasons.
        ///         The second way will advance the iterator earlier and may consume more items.
        ///     </para>
        /// </summary>
        /// <param name="source"></param>
        /// <param name="stride"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>



        public static StepBy<T> StepBy<T>(this IIterator<T> source, int stride)
        {
            return Iter.StepBy<T>.New(source, stride);
        }

        /// <summary>
        ///     <para>Takes two iterators and creates a new iterator over both in sequence.</para>
        ///     <para>
        ///         Chain() will return a new iterator which will first iterate over values
        ///         from the first iterator and then over values from the second iterator.
        ///     </para>
        ///     <para>In other words, it links two iterators together, in a chain.</para>
        ///     <para>Once is commonly used to adapt a single value into a chain of other kinds of iteration.</para>
        /// </summary>
        /// <param name="source"></param>
        /// <param name="other"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        // TODO: Extend Chain() to work with IIntoIterator and not just IIterator



        public static Chain<T> Chain<T>(this IIterator<T> source, IIterator<T> other)
        {
            return Iter.Chain<T>.New(source, other);
        }

        /// <summary>
        ///     <para>'Zips up' two iterators into a single iterator of pairs.</para>
        ///     <para>
        ///         Zip() returns a new iterator that will iterate over two other iterators, returning a
        ///         tuple where the first element comes from the first iterator, and the second element
        ///         comes from the second iterator.
        ///     </para>
        ///     <para>In other words, it zips two iterators together, into a single one.</para>
        ///     <para>If either iterator returns None, next from the zipped iterator will return None.</para>
        ///     <para>
        ///         If the first iterator returns None, zip will short-circuit and next will not be
        ///         called on the second iterator.
        ///     </para>
        /// </summary>
        /// <param name="source"></param>
        /// <param name="other"></param>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <returns></returns>



        public static Zip<T, U> Zip<T, U>(this IIterator<T> source, IIterator<U> other)
        {
            return Iter.Zip<T, U>.New(source, other);
        }

        /// <summary>
        ///     <para>Takes a closure and creates an iterator which calls that closure on each element.</para>
        ///     <para>
        ///         Map() transforms one iterator into another, by means of its argument.
        ///         It produces a new iterator which calls this closure on each element of the original iterator.
        ///     </para>
        ///     <para>
        ///         If you are good at thinking in types, you can think of Map() like this:
        ///         If you have an iterator that gives you elements of some type A,
        ///         and you want an iterator of some other type B, you can use Map(),
        ///         passing a closure that takes an A and returns a B.
        ///     </para>
        ///     <para>
        ///         Map() is conceptually similar to a for loop. However, as Map() is lazy,
        ///         it is best used when you're already working with other iterators.
        ///         If you're doing some sort of looping for a side effect,
        ///         it's considered more idiomatic to use for than Map().
        ///     </para>
        /// </summary>
        /// <param name="source"></param>
        /// <param name="f"></param>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <returns></returns>



        public static Map<T, U> Map<T, U>(this IIterator<T> source, Func<T, U> f)
        {
            return Iter.Map<T, U>.New(source, f);
        }

        /// <summary>
        ///     <para>Calls a closure on each element of an iterator.</para>
        ///     <para>
        ///         This is equivalent to using a for loop on the iterator,
        ///         although break and continue are not possible from a closure.
        ///     </para>
        ///     <para>
        ///         It's generally more idiomatic to use a for loop,
        ///         but ForEach may be more legible when processing items at the
        ///         end of longer iterator chains.
        ///     </para>
        /// </summary>
        /// <param name="source"></param>
        /// <param name="f"></param>
        /// <typeparam name="T"></typeparam>



        public static void ForEach<T>(this IIterator<T> source, Action<T> f)
        {
            for (var next = source.Next(); next.isSome; next = source.Next())
            {
                f(next.some);
            }
        }

        /// <summary>
        ///     <para>Creates an iterator which uses a closure to determine if an element should be yielded.</para>
        ///     <para>
        ///         The closure must return true or false. Filter() creates an iterator which calls this closure on each
        ///         element. If the closure returns true, then the element is returned. If the closure returns false, it
        ///         will try again, and call the closure on the next element, seeing if it passes the test.
        ///     </para>
        /// </summary>
        /// <param name="source"></param>
        /// <param name="f"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>



        public static Filter<T> Filter<T>(this IIterator<T> source, Func<T, bool> f)
        {
            return Iter.Filter<T>.New(source, f);
        }

        // TODO: Add a FilterMap<T, U>(IIterator<T>, Func<T, Option<U>>) -> FilterMap<T, U> method

        /// <summary>
        ///     <para>Creates an iterator which gives the current iteration count as well as the next value.</para>
        ///     <para>
        ///         The iterator returned yields pairs (i, val), where i is the current index of
        ///         iteration and val is the value returned by the iterator.
        ///     </para>
        ///     <para>
        ///         Enumerate() keeps its count as an int. If you want to count by a differently sized integer,
        ///         the Zip function provides similar functionality.
        ///     </para>
        /// </summary>
        /// <param name="source"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>



        public static Enumerate<T> Enumerate<T>(this IIterator<T> source)
        {
            return Iter.Enumerate<T>.New(source);
        }

        // TODO: Add a Peekable<T>(IIterator<T>) -> Peekable<T> method

        // TODO: Add a SkipWhile<T>(IIterator<T>, Func<T, bool>) -> SkipWhile<T> method

        // TODO: Add a TakeWhile<T>(IIterator<T>, Func<T, bool>) -> TakeWhile<T> method

        // TODO: Add a Skip<T>(IIterator<T>, int) -> Skip<T> method

        /// <summary>
        ///     Creates an iterator that yields its first n elements.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="n"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>



        public static Take<T> Take<T>(this IIterator<T> source, int n)
        {
            return Iter.Take<T>.New(source, n);
        }

        // TODO: Add a Scan<T, U, S>(IIterator<T>, S, Func<S, T, Option<U>>) -> Scan<T, U, S> method

        // TODO: Add a FlatMap<T, U>(IIterator<T>, Func<T, IIntoIterator<U>>) -> FlatMap<T, U> method

        // TODO: Add a Flatten<T>(IIterator<IIntoIterator<T>>) method

        /// <summary>
        ///     <para>Creates an iterator which ends after the first None.</para>
        ///     <para>After an iterator returns None, future calls may or may not yield Some(T) again.</para>
        ///     <para>Fuse() adapts an iterator, ensuring that after a None is given, it will always return None forever.</para>
        /// </summary>
        /// <param name="source"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>



        public static Fuse<T> Fuse<T>(this IIterator<T> source)
        {
            return Iter.Fuse<T>.New(source);
        }

        // TODO: Add an Inspect<T>(IIterator<T>, Action<T>) -> Inspect<T> method

        /// <summary>
        ///     Transforms an iterator into a collection.
        /// </summary>
        /// <param name="source"></param>
        /// <typeparam name="T">The type of the iterator item</typeparam>
        /// <typeparam name="C">A collection type that implements ICollection&lt;T&gt;</typeparam>
        /// <returns></returns>



        public static C Collect<T, C>(this IIterator<T> source) where C : ICollection<T>, new()
        {
            var buf = new C();

            for (var next = source.Next(); next.isSome; next = source.Next())
            {
                buf.Add(next.some);
            }

            return buf;
        }

        /// <summary>
        ///     Transforms an iterator into a collection. Specialised version for ReadOnlyCollections.
        /// </summary>
        /// <param name="source"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>




        public static ReadOnlyCollection<T> Collect<T>(this IIterator<T> source)
        {
            var buf = new ReadOnlyCollectionBuilder<T>();

            for (var next = source.Next(); next.isSome; next = source.Next())
            {
                buf.Add(next.some);
            }

            return buf.ToReadOnlyCollection();
        }

        /// <summary>
        ///     Transforms an iterator into a collection. Specialised version for dictionaries.
        /// </summary>
        /// <param name="source"></param>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <typeparam name="D"></typeparam>
        /// <returns></returns>




        public static D Collect<T, U, D>(this IIterator<(T, U)> source) where D : IDictionary<T, U>, new()
        {
            var buf = new D();

            for (var next = source.Next(); next.isSome; next = source.Next())
            {
                buf.Add(next.some.Item1, next.some.Item2);
            }

            return buf;
        }

        // TODO: Add a Partition<T, C>(IIterator<T>, Func<T, bool>) -> (C, C) method

        /// <summary>
        ///     <para>
        ///         An iterator method that applies a function as long as it returns successfully, producing a single,
        ///         final value.
        ///     </para>
        ///     <para>
        ///         TryFold() takes two arguments: an initial value, and a closure with two arguments: an 'accumulator',
        ///         and an element. The closure either returns successfully, with the value that the accumulator
        ///         should have for the next iteration, or it returns failure, with an error value that is propagated
        ///         back to the caller immediately (short-circuiting).
        ///     </para>
        ///     <para>
        ///         The initial value is the value the accumulator will have on the first call. If applying the closure
        ///         succeeded against every element of the iterator, TryFold() returns the final accumulator as success.
        ///     </para>
        ///     <para>Folding is useful whenever you have a collection of something, and want to produce a single value from it.</para>
        /// </summary>
        /// <param name="source"></param>
        /// <param name="init"></param>
        /// <param name="f"></param>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="S"></typeparam>
        /// <typeparam name="E"></typeparam>
        /// <returns></returns>




        public static Result<S, E> TryFold<T, S, E>(this IIterator<T> source, S init, Func<S, T, Result<S, E>> f)
        {
            var state = init;
            while (true)
            {
                var next = source.Next();
                if (!next.isSome)
                {
                    return Result<S, E>.Ok(state);
                }

                var stateUpdate = f(state, next.some);
                if (!stateUpdate.isOk)
                {
                    return stateUpdate;
                }

                state = stateUpdate.ok;
            }
        }

        /// <summary>
        ///     <para>
        ///         An iterator method that applies a function as long as it returns successfully, producing a single,
        ///         final value.
        ///     </para>
        ///     <para>
        ///         TryFold() takes two arguments: an initial value, and a closure with two arguments: an 'accumulator',
        ///         and an element. The closure either returns successfully, with the value that the accumulator
        ///         should have for the next iteration, or it returns failure, with an error value that is propagated
        ///         back to the caller immediately (short-circuiting).
        ///     </para>
        ///     <para>
        ///         The initial value is the value the accumulator will have on the first call. If applying the closure
        ///         succeeded against every element of the iterator, TryFold() returns the final accumulator as success.
        ///     </para>
        ///     <para>Folding is useful whenever you have a collection of something, and want to produce a single value from it.</para>
        /// </summary>
        /// <param name="source"></param>
        /// <param name="init"></param>
        /// <param name="f"></param>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="S"></typeparam>
        /// <returns></returns>




        public static Option<S> TryFold<T, S>(this IIterator<T> source, S init, Func<S, T, Option<S>> f)
        {
            var state = init;
            while (true)
            {
                var next = source.Next();
                if (!next.isSome)
                {
                    return Option<S>.Some(state);
                }

                var stateUpdate = f(state, next.some);
                if (!stateUpdate.isSome)
                {
                    return stateUpdate;
                }

                state = stateUpdate.some;
            }
        }

        /// <summary>
        ///     <para>
        ///         An iterator method that applies a fallible function to each item in the iterator, stopping at the first error
        ///         and returning that error.
        ///     </para>
        ///     <para>This can also be thought of as the fallible form of ForEach() or as the stateless version of TryFold().</para>
        /// </summary>
        /// <param name="source"></param>
        /// <param name="f"></param>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="E"></typeparam>
        /// <returns></returns>




        public static Result<EmptyTuple, E> TryForEach<T, E>(this IIterator<T> source, Func<T, Result<EmptyTuple, E>> f)
        {
            while (true)
            {
                var next = source.Next();
                if (!next.isSome)
                {
                    return Result<EmptyTuple, E>.Ok(EmptyTuple.Value);
                }

                var res = f(next.some);
                if (!res.isOk)
                {
                    return res;
                }
            }
        }

        /// <summary>
        ///     <para>An iterator method that applies a function, producing a single, final value.</para>
        ///     <para>
        ///         Fold() takes two arguments: an initial value, and a closure with two
        ///         arguments: an 'accumulator', and an element. The closure returns
        ///         the value that the accumulator should have for the next iteration.
        ///     </para>
        ///     <para>The initial value is the value the accumulator will have on the first call.</para>
        ///     <para>
        ///         After applying this closure to every element of the iterator,
        ///         Fold() returns the accumulator.
        ///     </para>
        ///     <para>This operation is sometimes called 'reduce' or 'inject'.</para>
        ///     <para>
        ///         Folding is useful whenever you have a collection of something,
        ///         and want to produce a single value from it.
        ///     </para>
        ///     <para>
        ///         Note: Fold(), and similar methods that traverse the entire iterator,
        ///         may not terminate for infinite iterators, even on traits for which a result is determinable in finite time.
        ///     </para>
        /// </summary>
        /// <param name="source"></param>
        /// <param name="init"></param>
        /// <param name="f"></param>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="S"></typeparam>
        /// <returns></returns>




        public static S Fold<T, S>(this IIterator<T> source, S init, Func<S, T, S> f)
        {
            var state = init;

            for (var next = source.Next(); next.isSome; next = source.Next())
            {
                state = f(state, next.some);
            }

            return state;
        }

        /// <summary>
        ///     <para>Tests if every element of the iterator matches a predicate.</para>
        ///     <para>
        ///         All() takes a closure that returns true or false. It applies this closure to each element of the iterator, and
        ///         if
        ///         they all return true, then so does All(). If any of them return false, it returns false.
        ///     </para>
        ///     <para>
        ///         All() is short-circuiting; in other words, it will stop processing as soon as it finds a false, given that no
        ///         matter what else happens, the result will also be false.
        ///         An empty iterator returns true.
        ///     </para>
        /// </summary>
        /// <param name="source"></param>
        /// <param name="f"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>




        public static bool All<T>(this IIterator<T> source, Func<T, bool> f)
        {
            for (var next = source.Next(); next.isSome; next = source.Next())
            {
                if (!f(next.some))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        ///     <para>Tests if any element of the iterator matches a predicate.</para>
        ///     <para>
        ///         Any() takes a closure that returns true or false. It applies this closure to each element of the iterator, and
        ///         if
        ///         any of them return true, then so does Any(). If they all return false, it returns false.
        ///     </para>
        ///     <para>
        ///         Any() is short-circuiting; in other words, it will stop processing as soon as it finds a true, given that no
        ///         matter
        ///         what else happens, the result will also be true.
        ///         An empty iterator returns false.
        ///     </para>
        /// </summary>
        /// <param name="source"></param>
        /// <param name="f"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>




        public static bool Any<T>(this IIterator<T> source, Func<T, bool> f)
        {
            for (var next = source.Next(); next.isSome; next = source.Next())
            {
                if (f(next.some))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        ///     <para>Searches for an element of an iterator that satisfies a predicate.</para>
        ///     <para>
        ///         Find() takes a closure that returns true or false. It applies this closure to each element of the iterator,
        ///         and if any of them return true, then Find() returns Some(element). If they all return false, it returns None.
        ///     </para>
        ///     <para>Find() is short-circuiting; in other words, it will stop processing as soon as the closure returns true.</para>
        /// </summary>
        /// <param name="source"></param>
        /// <param name="f"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>




        public static Option<T> Find<T>(this IIterator<T> source, Func<T, bool> f)
        {
            for (var next = source.Next(); next.isSome; next = source.Next())
            {
                if (f(next.some))
                {
                    return next;
                }
            }

            return Option<T>.None;
        }

        // TODO: Add a FindMap<T, U>(IIterator<T>, Func<T, Option<U>>) -> Option<U> method

        /// <summary>
        ///     <para>Searches for an element in an iterator, returning its index.</para>
        ///     <para>
        ///         Position() takes a closure that returns true or false. It applies this closure to each element of the
        ///         iterator, and if one of them returns true, then Position() returns Some(index). If all of them return false, it
        ///         returns None.
        ///     </para>
        ///     <para>Position() is short-circuiting; in other words, it will stop processing as soon as it finds a true.</para>
        /// </summary>
        /// <param name="source"></param>
        /// <param name="f"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>




        public static Option<int> Position<T>(this IIterator<T> source, Func<T, bool> f)
        {
            var i = 0;

            for (var next = source.Next(); next.isSome; next = source.Next())
            {
                if (f(next.some))
                {
                    return Option<int>.Some(i);
                }

                i += 1;
            }

            return Option<int>.None;
        }

        /// <summary>
        ///     <para>Returns the maximum element of an iterator.</para>
        ///     <para>
        ///         If several elements are equally maximum, the last element is returned. If the iterator is empty, None is
        ///         returned.
        ///     </para>
        /// </summary>
        /// <param name="source"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>




        public static Option<T> Max<T>(this IIterator<T> source)
            where T : IComparable<T>
        {
            var max = Option<T>.None;

            for (var next = source.Next(); next.isSome; next = source.Next())
            {
                if (!max.isSome || next.some.CompareTo(max.some) >= 0)
                {
                    max = next;
                }
            }

            return max;
        }

        /// <summary>
        ///     <para>Returns the minimum element of an iterator.</para>
        ///     <para>
        ///         If several elements are equally minimum, the first element is returned. If the iterator is empty, None is
        ///         returned.
        ///     </para>
        /// </summary>
        /// <param name="source"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>




        public static Option<T> Min<T>(this IIterator<T> source)
            where T : IComparable<T>
        {
            var min = Option<T>.None;

            for (var next = source.Next(); next.isSome; next = source.Next())
            {
                if (!min.isSome || next.some.CompareTo(min.some) < 0)
                {
                    min = next;
                }
            }

            return min;
        }

        // TODO: Add a Rev<T>(IDoubleEndedIterator<T>) -> Rev<T> method

        // TODO: Add a Unzip<T, U, C, D>(IIterator<(T, U)>) -> (C, D) method

        // TODO: Add a Cloned<T>(IIterator<T>) -> Cloned<T> where T: IClone<T>

        // TODO: Add a Cycle<T>(IIterator<T>) -> Cycle<T> method - needs IIterator<T>: IClone<IIterator<T>>

        /// <summary>
        ///     Calculates a sensible deep hash code by calling GetHashCode()
        ///     on every element in the iterator and combining them.
        /// </summary>
        /// <param name="source"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>




        public static int Hash<T>(this IIterator<T> source)
        {
            var hashCode = 1;

            for (var next = source.Next(); next.isSome; next = source.Next())
            {
                hashCode = (hashCode * 397) ^ next.some.GetHashCode();
            }

            return hashCode;
        }

        /// <summary>
        ///     Determines if the elements of this Iterator are equal to those of another with respect to the specified equality
        ///     function.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="other"></param>
        /// <param name="f"></param>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <returns></returns>




        public static bool EqBy<T, U>(this IIterator<T> source, IIterator<U> other, Func<T, U, bool> f)
            where T : IEquatable<U>
        {
            while (true)
            {
                var nextSource = source.Next();
                if (!nextSource.isSome)
                {
                    return !other.Next().isSome;
                }

                var innerSource = nextSource.some;

                var nextOther = other.Next();
                if (!nextOther.isSome)
                {
                    return false;
                }

                var innerOther = nextOther.some;

                if (!f(innerSource, innerOther))
                {
                    return false;
                }
            }
        }

        /// <summary>
        ///     Determines if the elements of this Iterator are equal to those of another.
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <returns></returns>




        public static bool Eq<T, U>(this IIterator<T> left, IIterator<U> right)
            where T : IEquatable<U>
        {
            while (true)
            {
                var nextSource = left.Next();
                if (!nextSource.isSome)
                {
                    return !right.Next().isSome;
                }

                var innerSource = nextSource.some;

                var nextOther = right.Next();
                if (!nextOther.isSome)
                {
                    return false;
                }

                var innerOther = nextOther.some;

                if (!innerSource.Equals(innerOther))
                {
                    return false;
                }
            }
        }

        /// <summary>
        ///     Determines if the elements of this Iterator are unequal to those of another.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="other"></param>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <returns></returns>




        public static bool Ne<T, U>(this IIterator<T> source, IIterator<U> other)
            where T : IEquatable<U>
        {
            return !source.Eq(other);
        }

        /// <summary>
        ///     Determines if the elements of this Iterator are lexicographically less than those of another.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="other"></param>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <returns></returns>




        public static bool Lt<T, U>(this IIterator<T> source, IIterator<U> other)
            where T : IComparable<U>
        {
            return source.Zip(other).All(e => e.Item1.CompareTo(e.Item2) < 0);
        }

        /// <summary>
        ///     Determines if the elements of this Iterator are lexicographically less or equal to those of another.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="other"></param>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <returns></returns>




        public static bool Le<T, U>(this IIterator<T> source, IIterator<U> other)
            where T : IComparable<U>
        {
            return source.Zip(other).All(e => e.Item1.CompareTo(e.Item2) <= 0);
        }

        /// <summary>
        ///     Determines if the elements of this Iterator are lexicographically greater than those of another.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="other"></param>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <returns></returns>




        public static bool Gt<T, U>(this IIterator<T> source, IIterator<U> other)
            where T : IComparable<U>
        {
            return source.Zip(other).All(e => e.Item1.CompareTo(e.Item2) > 0);
        }

        /// <summary>
        ///     Determines if the elements of this Iterator are lexicographically greater than or equal to those of another.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="other"></param>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <returns></returns>




        public static bool Ge<T, U>(this IIterator<T> source, IIterator<U> other)
            where T : IComparable<U>
        {
            return source.Zip(other).All(e => e.Item1.CompareTo(e.Item2) >= 0);
        }

        /// <summary>
        ///     Sums the elements of an iterator.
        ///     Takes each element, adds them together, and returns the result.
        ///     An empty iterator returns the zero value of the type.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>




        public static int Sum(this IIterator<int> source)
        {
            var state = 0;

            for (var next = source.Next(); next.isSome; next = source.Next())
            {
                state += next.some;
            }

            return state;
        }

        /// <summary>
        ///     Sums the elements of an iterator.
        ///     Takes each element, adds them together, and returns the result.
        ///     An empty iterator returns the zero value of the type.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>




        public static uint Sum(this IIterator<uint> source)
        {
            uint state = 0;

            for (var next = source.Next(); next.isSome; next = source.Next())
            {
                state += next.some;
            }

            return state;
        }

        /// <summary>
        ///     Sums the elements of an iterator.
        ///     Takes each element, adds them together, and returns the result.
        ///     An empty iterator returns the zero value of the type.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>




        public static long Sum(this IIterator<long> source)
        {
            long state = 0;

            for (var next = source.Next(); next.isSome; next = source.Next())
            {
                state += next.some;
            }

            return state;
        }

        /// <summary>
        ///     Sums the elements of an iterator.
        ///     Takes each element, adds them together, and returns the result.
        ///     An empty iterator returns the zero value of the type.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>




        public static ulong Sum(this IIterator<ulong> source)
        {
            ulong state = 0;

            for (var next = source.Next(); next.isSome; next = source.Next())
            {
                state += next.some;
            }

            return state;
        }

        /// <summary>
        ///     Sums the elements of an iterator.
        ///     Takes each element, adds them together, and returns the result.
        ///     An empty iterator returns the zero value of the type.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>




        public static float Sum(this IIterator<float> source)
        {
            float state = 0;

            for (var next = source.Next(); next.isSome; next = source.Next())
            {
                state += next.some;
            }

            return state;
        }

        /// <summary>
        ///     Sums the elements of an iterator.
        ///     Takes each element, adds them together, and returns the result.
        ///     An empty iterator returns the zero value of the type.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>




        public static double Sum(this IIterator<double> source)
        {
            double state = 0;

            for (var next = source.Next(); next.isSome; next = source.Next())
            {
                state += next.some;
            }

            return state;
        }

        /// <summary>
        ///     Sums the elements of an iterator.
        ///     Takes each element, adds them together, and returns the result.
        ///     An empty iterator returns the zero value of the type.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>




        public static decimal Sum(this IIterator<decimal> source)
        {
            decimal state = 0;

            for (var next = source.Next(); next.isSome; next = source.Next())
            {
                state += next.some;
            }

            return state;
        }

        /// <summary>
        ///     Iterates over the entire iterator, multiplying all the elements
        ///     An empty iterator returns the one value of the type.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>




        public static int Product(this IIterator<int> source)
        {
            var state = 1;

            for (var next = source.Next(); next.isSome; next = source.Next())
            {
                state *= next.some;
            }

            return state;
        }

        /// <summary>
        ///     Iterates over the entire iterator, multiplying all the elements
        ///     An empty iterator returns the one value of the type.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>




        public static uint Product(this IIterator<uint> source)
        {
            uint state = 1;

            for (var next = source.Next(); next.isSome; next = source.Next())
            {
                state *= next.some;
            }

            return state;
        }

        /// <summary>
        ///     Iterates over the entire iterator, multiplying all the elements
        ///     An empty iterator returns the one value of the type.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>




        public static long Product(this IIterator<long> source)
        {
            long state = 1;

            for (var next = source.Next(); next.isSome; next = source.Next())
            {
                state *= next.some;
            }

            return state;
        }

        /// <summary>
        ///     Iterates over the entire iterator, multiplying all the elements
        ///     An empty iterator returns the one value of the type.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>




        public static ulong Product(this IIterator<ulong> source)
        {
            ulong state = 1;

            for (var next = source.Next(); next.isSome; next = source.Next())
            {
                state *= next.some;
            }

            return state;
        }

        /// <summary>
        ///     Iterates over the entire iterator, multiplying all the elements
        ///     An empty iterator returns the one value of the type.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>




        public static float Product(this IIterator<float> source)
        {
            float state = 1;

            for (var next = source.Next(); next.isSome; next = source.Next())
            {
                state *= next.some;
            }

            return state;
        }

        /// <summary>
        ///     Iterates over the entire iterator, multiplying all the elements
        ///     An empty iterator returns the one value of the type.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>




        public static double Product(this IIterator<double> source)
        {
            double state = 1;

            for (var next = source.Next(); next.isSome; next = source.Next())
            {
                state *= next.some;
            }

            return state;
        }

        /// <summary>
        ///     Iterates over the entire iterator, multiplying all the elements
        ///     An empty iterator returns the one value of the type.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>




        public static decimal Product(this IIterator<decimal> source)
        {
            decimal state = 1;

            for (var next = source.Next(); next.isSome; next = source.Next())
            {
                state *= next.some;
            }

            return state;
        }
    }
}
