namespace Koboldgames.Iter
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Collects extension methods that allow creating iterators from standard
    /// library collections such as <c>IEnumerable</c>, <c>IReadOnlyCollection</c>,
    /// <c>IReadOnlyList</c>, and <c>IReadOnlyDictionary</c>.
    /// </summary>
    public static class CollectionExtensions
    {
        /// <summary>
        /// Create an iterator over an IEnumerable
        /// </summary>
        /// <param name="source"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static EnumerableIter<T> Iter<T>(this IEnumerable<T> source)
        {
            return EnumerableIter<T>.New(source);
        }

        /// <summary>
        /// Create an iterator over an IEnumerator
        /// </summary>
        /// <param name="source"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static EnumeratorIter<T> Iter<T>(this IEnumerator<T> source)
        {
            return EnumeratorIter<T>.New(source);
        }

        /// <summary>
        /// Create an iterator over an IReadOnlyCollection
        /// </summary>
        /// <param name="source"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static ReadOnlyCollectionIter<T> Iter<T>(this IReadOnlyCollection<T> source)
        {
            return ReadOnlyCollectionIter<T>.New(source);
        }

        /// <summary>
        /// Create an iterator over an IReadOnlyList
        /// </summary>
        /// <param name="source"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static ReadOnlyListIter<T> Iter<T>(this IReadOnlyList<T> source)
        {
            return ReadOnlyListIter<T>.New(source);
        }

        /// <summary>
        /// Create an iterator over an IReadOnlyDictionary
        /// </summary>
        /// <param name="source"></param>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <returns></returns>
        public static ReadOnlyDictionaryIter<T, U> Iter<T, U>(this IReadOnlyDictionary<T, U> source)
        {
            return ReadOnlyDictionaryIter<T, U>.New(source);
        }
    }
}
