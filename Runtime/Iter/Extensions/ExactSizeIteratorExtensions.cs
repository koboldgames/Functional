namespace Koboldgames.Iter
{
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Collects extension methods to IExactSizeIteratorIterator that come "for
    /// free" when you implement IExactSizeIterator.
    /// </summary>
    public static class ExactSizeIteratorExtensions
    {
        /// <summary>
        ///     Transforms an iterator into a collection. Specialised version for arrays.
        /// </summary>
        /// <param name="source"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>



        public static T[] Collect<T>(this IExactSizeIterator<T> source)
        {
            int len = source.Length();
            var buf = new T[len];

            for (var i = 0; i < len; i += 1)
            {
                buf[i] = source.Next().some;
            }

            return buf;
        }

        /// <summary>
        /// Sums the elements of an iterator.
        ///
        /// Takes each element, adds them together, and returns the result.
        ///
        /// An empty iterator returns the zero value of the type.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>



        public static int Sum(this IExactSizeIterator<int> source)
        {
            int state = 0;
            int len = source.Length();

            for (int i = 0; i < len; i += 1)
            {
                state += source.Next().some;
            }

            return state;
        }

        /// <summary>
        /// Sums the elements of an iterator.
        ///
        /// Takes each element, adds them together, and returns the result.
        ///
        /// An empty iterator returns the zero value of the type.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>



        public static uint Sum(this IExactSizeIterator<uint> source)
        {
            uint state = 0;
            int len = source.Length();

            for (int i = 0; i < len; i += 1)
            {
                state += source.Next().some;
            }

            return state;
        }

        /// <summary>
        /// Sums the elements of an iterator.
        ///
        /// Takes each element, adds them together, and returns the result.
        ///
        /// An empty iterator returns the zero value of the type.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>



        public static long Sum(this IExactSizeIterator<long> source)
        {
            long state = 0;
            int len = source.Length();

            for (int i = 0; i < len; i += 1)
            {
                state += source.Next().some;
            }

            return state;
        }

        /// <summary>
        /// Sums the elements of an iterator.
        ///
        /// Takes each element, adds them together, and returns the result.
        ///
        /// An empty iterator returns the zero value of the type.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>



        public static ulong Sum(this IExactSizeIterator<ulong> source)
        {
            ulong state = 0;
            int len = source.Length();

            for (int i = 0; i < len; i += 1)
            {
                state += source.Next().some;
            }

            return state;
        }

        /// <summary>
        /// Sums the elements of an iterator.
        ///
        /// Takes each element, adds them together, and returns the result.
        ///
        /// An empty iterator returns the zero value of the type.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>



        public static float Sum(this IExactSizeIterator<float> source)
        {
            float state = 0;
            int len = source.Length();

            for (int i = 0; i < len; i += 1)
            {
                state += source.Next().some;
            }

            return state;
        }

        /// <summary>
        /// Sums the elements of an iterator.
        ///
        /// Takes each element, adds them together, and returns the result.
        ///
        /// An empty iterator returns the zero value of the type.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>



        public static double Sum(this IExactSizeIterator<double> source)
        {
            double state = 0;
            int len = source.Length();

            for (int i = 0; i < len; i += 1)
            {
                state += source.Next().some;
            }

            return state;
        }

        /// <summary>
        /// Sums the elements of an iterator.
        ///
        /// Takes each element, adds them together, and returns the result.
        ///
        /// An empty iterator returns the zero value of the type.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>



        public static decimal Sum(this IExactSizeIterator<decimal> source)
        {
            decimal state = 0;
            int len = source.Length();

            for (int i = 0; i < len; i += 1)
            {
                state += source.Next().some;
            }

            return state;
        }

        /// <summary>
        /// Iterates over the entire iterator, multiplying all the elements
        ///
        /// An empty iterator returns the one value of the type.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>



        public static int Product(this IExactSizeIterator<int> source)
        {
            int state = 1;
            int len = source.Length();

            for (int i = 0; i < len; i += 1)
            {
                state *= source.Next().some;
            }

            return state;
        }

        /// <summary>
        /// Iterates over the entire iterator, multiplying all the elements
        ///
        /// An empty iterator returns the one value of the type.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>



        public static uint Product(this IExactSizeIterator<uint> source)
        {
            uint state = 1;
            int len = source.Length();

            for (int i = 0; i < len; i += 1)
            {
                state *= source.Next().some;
            }

            return state;
        }

        /// <summary>
        /// Iterates over the entire iterator, multiplying all the elements
        ///
        /// An empty iterator returns the one value of the type.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>



        public static long Product(this IExactSizeIterator<long> source)
        {
            long state = 1;
            int len = source.Length();

            for (int i = 0; i < len; i += 1)
            {
                state *= source.Next().some;
            }

            return state;
        }

        /// <summary>
        /// Iterates over the entire iterator, multiplying all the elements
        ///
        /// An empty iterator returns the one value of the type.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>



        public static ulong Product(this IExactSizeIterator<ulong> source)
        {
            ulong state = 1;
            int len = source.Length();

            for (int i = 0; i < len; i += 1)
            {
                state *= source.Next().some;
            }

            return state;
        }

        /// <summary>
        /// Iterates over the entire iterator, multiplying all the elements
        ///
        /// An empty iterator returns the one value of the type.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>



        public static float Product(this IExactSizeIterator<float> source)
        {
            float state = 1;
            int len = source.Length();

            for (int i = 0; i < len; i += 1)
            {
                state *= source.Next().some;
            }

            return state;
        }

        /// <summary>
        /// Iterates over the entire iterator, multiplying all the elements
        ///
        /// An empty iterator returns the one value of the type.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>



        public static double Product(this IExactSizeIterator<double> source)
        {
            double state = 1;
            int len = source.Length();

            for (int i = 0; i < len; i += 1)
            {
                state *= source.Next().some;
            }

            return state;
        }

        /// <summary>
        /// Iterates over the entire iterator, multiplying all the elements
        ///
        /// An empty iterator returns the one value of the type.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>



        public static decimal Product(this IExactSizeIterator<decimal> source)
        {
            decimal state = 1;
            int len = source.Length();

            for (int i = 0; i < len; i += 1)
            {
                state *= source.Next().some;
            }

            return state;
        }
    }
}
