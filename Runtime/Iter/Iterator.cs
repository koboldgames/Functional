namespace Koboldgames.Iter
{
    using System;
    using Option;

    /// <summary>
    /// Contains methods that create from-scratch iterators with specific properties
    /// </summary>
    public static class Iterator
    {
        /// <summary>
        /// Creates an iterator that yields nothing.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static Empty<T> Empty<T>()
        {
            return Koboldgames.Iter.Empty<T>.New();
        }

        /// <summary>
        /// <para>Creates an iterator that yields an element exactly once.</para>
        /// <para>This is commonly used to adapt a single value into a chain of other kinds of iteration.
        /// Maybe you have an iterator that covers almost everything, but you need an extra special case.
        /// Maybe you have a function which works on iterators, but you only need to process one value.</para>
        /// </summary>
        /// <param name="onceValue"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static Once<T> Once<T>(T onceValue)
        {
            return Koboldgames.Iter.Once<T>.New(onceValue);
        }

        /// <summary>
        /// <para>Creates a new iterator that endlessly repeats a single element.</para>
        /// <para>The repeat() function repeats a single value over and over again.</para>
        /// <para>Infinite iterators like repeat() are often used with adapters
        /// like take, in order to make them finite.</para>
        /// <para>If the element type of the iterator you need does not implement Clone,
        /// or if you do not want to keep the repeated element in memory, you can
        /// instead use the repeat_with function.</para>
        /// </summary>
        /// <param name="repeatValue"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static Repeat<T> Repeat<T>(T repeatValue)
        {
            return Koboldgames.Iter.Repeat<T>.New(repeatValue);
        }

        /// <summary>
        /// Creates an integer interval iterator within [start, end),
        /// that is inclusive start, and exclusive end.
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public static Range Range(int start, int end)
        {
            return Koboldgames.Iter.Range.New(start, end);
        }

        /// <summary>
        /// Creates an iterator that returns some value every even iteration, and none every odd one.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public static FlipFlop<T> FlipFlop<T>(T flipValue)
        {
            return Koboldgames.Iter.FlipFlop<T>.New(flipValue);
        }

        /// <summary>
        /// <para>Creates a new iterator where each iteration calls the provided closure F: FnMut() -> Option&lt;T&gt;.</para>
        /// <para>This allows creating a custom iterator with any behavior without using the
        /// more verbose syntax of creating a dedicated type and implementing the Iterator trait for it.</para>
        /// <para>Note that the FromFn iterator doesn’t make assumptions about the behavior
        /// of the closure, and therefore conservatively does not implement FusedIterator,
        /// or override Iterator::size_hint from its default (0, None).</para>
        /// <para>The closure can use captures and its environment to track state across iterations.
        /// Depending on how the iterator is used, this may require specifying the move keyword on the closure.</para>
        /// </summary>
        /// <param name="f"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static FromFunc<T> FromFunc<T>(Func<Option<T>> f)
        {
            return Koboldgames.Iter.FromFunc<T>.New(f);
        }
    }
}
