namespace Koboldgames.Iter
{
    /// <summary>
    /// <para>An iterator that always continues to yield None when exhausted.</para>
    /// <para>Calling next on a fused iterator that has returned None once is guaranteed to return None again. This trait should be implemented by all iterators that behave this way because it allows optimizing Fuse.</para>
    /// <para>Note: In general, you should not use FusedIterator in generic bounds if you need a fused iterator. Instead, you should just call Fuse on the iterator. If the iterator is already fused, the additional Fuse wrapper will be a no-op with no performance penalty.</para>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IFusedIterator<T> : IIterator<T>
    {
    }
}
