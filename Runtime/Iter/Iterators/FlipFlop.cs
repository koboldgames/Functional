namespace Koboldgames.Iter
{
    using System.Runtime.CompilerServices;
    using Option;

    /// <summary>
    /// An iterator that returns some value every even iteration, and none every odd one.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public struct FlipFlop<T> : IIterator<T>
    {
        private T flipValue;
        private bool flipping;

        /// <summary>
        /// Creates an iterator that returns some value every even iteration, and none every odd one.
        /// </summary>
        internal static FlipFlop<T> New(T flipValue)
        {
            Koboldgames.Iter.FlipFlop<T> v;
            v.flipValue = flipValue;
            v.flipping = true;
            return v;
        }
        /// <inheritdoc />

        public Option<T> Next()
        {
            if (flipping)
            {
                flipping = false;
                return Option<T>.Some(flipValue);
            }

            flipping = true;
            return Option<T>.None;
        }

        /// <inheritdoc />
        public (int, Option<int>) SizeHint()
        {
            return (int.MaxValue, Option<int>.None);
        }
    }
}
