namespace Koboldgames.Iter
{
    using System;
    using System.Runtime.CompilerServices;
    using Option;

    /// <summary>
    /// <para>An iterator that iterates two other iterators simultaneously.</para>
    /// <para>This struct is created by the Zip method on IIterator. See its documentation for more.</para>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="U"></typeparam>
    public struct Zip<T, U> : IIterator<(T, U)>
    {
        private IIterator<T> left;
        private IIterator<U> right;

        /// <summary>
        ///     <para>'Zips up' two iterators into a single iterator of pairs.</para>
        ///     <para>Zip() returns a new iterator that will iterate over two other iterators, returning a
        ///     tuple where the first element comes from the first iterator, and the second element
        ///     comes from the second iterator.</para>
        ///     <para>In other words, it zips two iterators together, into a single one.</para>
        ///     <para>If either iterator returns None, next from the zipped iterator will return None.</para>
        ///     <para>If the first iterator returns None, zip will short-circuit and next will not be
        ///     called on the second iterator.</para>
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>

        internal static Zip<T, U> New(IIterator<T> left, IIterator<U> right)
        {
            Zip<T, U> v;
            v.left = left;
            v.right = right;
            return v;
        }

        /// <inheritdoc />

        public Option<(T, U)> Next()
        {
            Option<T> nextLeft = left.Next();
            if (!nextLeft.isSome)
            {
                return Option<(T, U)>.None;
            }

            Option<U> nextRight = right.Next();
            if (!nextRight.isSome)
            {
                return Option<(T, U)>.None;
            }

            return Option<(T, U)>.Some((nextLeft.some, nextRight.some));
        }

        /// <inheritdoc />
        public (int, Option<int>) SizeHint()
        {
            (int lowLeft, Option<int> highLeft) = left.SizeHint();
            (int lowRight, Option<int> highRight) = right.SizeHint();

            int low = Math.Min(lowLeft, lowRight);

            Option<int> high;
            if (highLeft.IsSome() && highRight.IsSome())
            {
                high = Option<int>.Some(Math.Min(highLeft.some, highRight.some));
            }
            else if (highLeft.IsSome() && highRight.IsNone())
            {
                high = highLeft;
            }
            else if (highLeft.IsNone() && highRight.IsSome())
            {
                high = highRight;
            }
            else
            {
                high = Option<int>.None;
            }

            return (low, high);
        }
    }
}
