using System;
using System.Runtime.CompilerServices;
using Koboldgames.Option;

namespace Koboldgames.Iter
{
    /// <summary>
    /// Creates an integer interval iterator within [start, end),
    /// that is inclusive start, and exclusive end.
    /// </summary>
    public struct Range : IExactSizeIterator<int>
    {
        private int start;
        private int end;
        private int index;

        /// <summary>
        /// Creates an integer interval iterator within [start, end),
        /// that is inclusive start, and exclusive end.
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        internal static Range New(int start, int end)
        {
            Range v;
            v.start = start;
            v.end = end;
            v.index = start;
            return v;
        }

        /// <inheritdoc />

        public Option<int> Next()
        {
            if (index >= end)
            {
                return Option<int>.None;
            }

            var tmp =  Option<int>.Some(index);
            index += 1;
            return tmp;
        }

        /// <inheritdoc />

        public int Length() => SizeHint().Item1;

        /// <inheritdoc />

        public bool IsEmpty() => Length() == 0;

        /// <inheritdoc />
        public (int, Option<int>) SizeHint()
        {
            int remainingLen = Math.Max(0, end - start) - index;
            return (remainingLen, Option<int>.Some(remainingLen));
        }
    }
}
