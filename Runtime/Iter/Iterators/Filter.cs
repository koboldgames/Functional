using System;
using System.Runtime.CompilerServices;
using Koboldgames.Option;

namespace Koboldgames.Iter
{
    /// <summary>
    /// <para>An iterator that filters the elements of iter with predicate.</para>
    /// <para>This struct is created by the Filter method on IIterator. See its documentation for more.</para>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public struct Filter<T> : IIterator<T>
    {
        private Func<T, bool> predicate;
        private IIterator<T> source;

        /// <summary>
        ///     <para>Creates an iterator which uses a closure to determine if an element should be yielded.</para>
        ///     <para>The closure must return true or false. Filter() creates an iterator which calls this closure on each
        ///     element. If the closure returns true, then the element is returned. If the closure returns false, it
        ///     will try again, and call the closure on the next element, seeing if it passes the test.</para>
        /// </summary>
        /// <param name="source"></param>
        /// <param name="f"></param>
        /// <returns></returns>

        internal static Filter<T> New(IIterator<T> source, Func<T, bool> f)
        {
            Filter<T> v;
            v.source = source;
            v.predicate = f;
            return v;
        }

        /// <inheritdoc />

        public Option<T> Next()
        {
            for (var n = source.Next(); n.isSome; n = source.Next())
            {
                if (predicate(n.some))
                {
                    return n;
                }
            }

            return Option<T>.None;
        }

        /// <inheritdoc />
        public (int, Option<int>) SizeHint()
        {
            return (0, source.SizeHint().Item2);
        }
    }
}
