namespace Koboldgames.Iter
{
    using System;
    using System.Runtime.CompilerServices;
    using Option;

    /// <summary>
    /// <para>An iterator where each iteration calls the provided closure.</para>
    /// <para>This struct is created by the Iterator.FromFunc function. See its documentation for more.</para>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public struct FromFunc<T> : IIterator<T>
    {
        private Func<Option<T>> f;

        /// <summary>
        /// <para>Creates a new iterator where each iteration calls the provided closure F: FnMut() -> Option&lt;T&gt;.</para>
        /// <para>This allows creating a custom iterator with any behavior without using the
        /// more verbose syntax of creating a dedicated type and implementing the Iterator trait for it.</para>
        /// <para>Note that the FromFn iterator doesn’t make assumptions about the behavior
        /// of the closure, and therefore conservatively does not implement FusedIterator,
        /// or override Iterator::size_hint from its default (0, None).</para>
        /// <para>The closure can use captures and its environment to track state across iterations.
        /// Depending on how the iterator is used, this may require specifying the move keyword on the closure.</para>
        /// </summary>
        /// <param name="f"></param>
        /// <returns></returns>
        internal static FromFunc<T> New(Func<Option<T>> f)
        {
            Koboldgames.Iter.FromFunc<T> v;
            v.f = f;
            return v;
        }

        /// <inheritdoc />

        public Option<T> Next()
        {
            return f();
        }

        /// <inheritdoc />
        public (int, Option<int>) SizeHint()
        {
            return (0, Option<int>.None);
        }
    }
}
