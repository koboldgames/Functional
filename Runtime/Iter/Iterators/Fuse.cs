namespace Koboldgames.Iter
{
    using System.Runtime.CompilerServices;
    using Option;

    /// <summary>
    /// <para>An iterator that yields None forever after the underlying iterator yields None once.</para>
    /// <para>This struct is created by the Fuse method on IIterator. See its documentation for more.</para>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public struct Fuse<T> : IFusedIterator<T>
    {
        private IIterator<T> source;
        private bool fused;

        /// <summary>
        ///     <para>Creates an iterator which ends after the first None.</para>
        ///     <para>After an iterator returns None, future calls may or may not yield Some(T) again.</para>
        ///     <para>Fuse() adapts an iterator, ensuring that after a None is given, it will always return None forever.</para>
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>

        internal static Fuse<T> New(IIterator<T> source)
        {
            Fuse<T> v;
            v.source = source;
            v.fused = false;
            return v;
        }

        /// <inheritdoc />

        public Option<T> Next()
        {
            if (fused)
            {
                return Option<T>.None;
            }

            Option<T> next = source.Next();
            if (!next.isSome)
            {
                fused = true;
                return Option<T>.None;
            }

            return next;
        }

        /// <inheritdoc />
        public (int, Option<int>) SizeHint()
        {
            return fused ? (0, Option<int>.Some(0)) : source.SizeHint();
        }
    }
}
