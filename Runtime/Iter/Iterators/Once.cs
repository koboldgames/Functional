namespace Koboldgames.Iter
{
    using System.Runtime.CompilerServices;
    using Option;

    /// <summary>
    /// <para>An iterator that yields an element exactly once.</para>
    /// <para>This class is created by the Iterator.Once function. See its documentation for more.</para>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public struct Once<T> : IExactSizeIterator<T>
    {
        private T onceValue;
        private bool called;

        /// <summary>
        /// <para>Creates an iterator that yields an element exactly once.</para>
        /// <para>This is commonly used to adapt a single value into a chain of other kinds of iteration.
        /// Maybe you have an iterator that covers almost everything, but you need an extra special case.
        /// Maybe you have a function which works on iterators, but you only need to process one value.</para>
        /// </summary>
        /// <param name="onceValue"></param>
        /// <returns></returns>
        internal static Once<T> New(T onceValue)
        {
            Once<T> v;
            v.onceValue = onceValue;
            v.called = false;
            return v;
        }

        /// <inheritdoc />

        public int Length() => SizeHint().Item1;

        /// <inheritdoc />

        public bool IsEmpty() => Length() == 0;

        /// <inheritdoc />

        public Option<T> Next()
        {
            if (called)
            {
                return Option<T>.None;
            }

            called = true;
            return Option<T>.Some(onceValue);
        }

        /// <inheritdoc />
        public (int, Option<int>) SizeHint()
        {
            return (1, Option<int>.Some(1));
        }
    }
}
