namespace Koboldgames.Iter
{
    using System;
    using System.Runtime.CompilerServices;
    using Option;

    /// <summary>
    /// <para>An iterator that only iterates over the first n iterations of iter.</para>
    /// <para>This struct is created by the Take method on IIterator. See its documentation for more.</para>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public struct Take<T> : IIterator<T>
    {
        private IIterator<T> source;
        private int size;
        private int counter;

        /// <summary>
        /// Creates an iterator that yields its first n elements.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="n"></param>
        /// <returns></returns>

        internal static Take<T> New(IIterator<T> source, int n)
        {
            Take<T> v;
            v.source = source;
            v.size = n;
            v.counter = 0;
            return v;
        }

        /// <inheritdoc />

        public Option<T> Next()
        {
            if (counter >= size)
            {
                return Option<T>.None;
            }

            counter += 1;
            return source.Next();
        }

        /// <inheritdoc />
        public (int, Option<int>) SizeHint()
        {
            int size = this.size;

            if (size == 0)
            {
                return (0, Option<int>.Some(0));
            }

            (int low, Option<int> high) = source.SizeHint();

            low = Math.Min(low, size);
            high = high.Filter(h => h < size).Or(Option<int>.Some(size));

            return (low, high);
        }
    }
}
