namespace Koboldgames.Iter
{
    using System;
    using System.Runtime.CompilerServices;
    using Option;

    /// <summary>
    /// <para>An iterator for stepping iterators by a custom amount.</para>
    /// <para>This struct is created by the StepBy method on IIterator. See its documentation for more.</para>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public struct StepBy<T> : IIterator<T>
    {
        private IIterator<T> source;
        private int n;
        private bool firstTake;

        /// <summary>
        /// <para>Creates an iterator starting at the same point, but stepping by the given amount at each iteration.</para>
        /// <para>Note 1: The first element of the iterator will always be returned, regardless of the step given.</para>
        /// <para>Note 2: The time at which ignored elements are pulled is not fixed. StepBy behaves like the sequence
        /// Next(), Nth(step-1), Nth(step-1), …, but is also free to behave like the sequence AdvanceNAndReturnFirst(step),
        /// AdvanceNAndReturnFirst(step), … Which way is used may change for some iterators for performance reasons.
        /// The second way will advance the iterator earlier and may consume more items.</para>
        /// </summary>
        /// <param name="source"></param>
        /// <param name="stride"></param>
        /// <returns></returns>

        internal static StepBy<T> New(IIterator<T> source, int stride)
        {
            StepBy<T> v;
            v.source = source;
            v.n = stride - 1;
            v.firstTake = true;
            return v;
        }

        /// <inheritdoc />

        public Option<T> Next()
        {
            if (firstTake)
            {
                firstTake = false;
                return source.Next();
            }

            return source.Nth(n);
        }

        /// <inheritdoc />
        public (int, Option<int>) SizeHint()
        {
            Func<int, int> FirstSize(int step)
            {
                return x => x == 0 ? 0 : 1 + (x - 1) / (step + 1);
            }

            Func<int, int> SubsequentSize(int step)
            {
                return x => x / (step + 1);
            }

            (int low, Option<int> high) = source.SizeHint();

            if (firstTake)
            {
                Func<int, int> f = FirstSize(n);
                return (f(low), high.Map(f));
            }
            else
            {
                Func<int, int> f = SubsequentSize(n);
                return (f(low), high.Map(f));
            }
        }
    }
}
