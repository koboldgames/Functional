namespace Koboldgames.Iter
{
    using System.Runtime.CompilerServices;
    using Option;

    /// <summary>
    /// <para>An iterator that repeats an element endlessly.</para>
    /// <para>This class is created by the Iterator.Repeat function. See its documentation for more.</para>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public struct Repeat<T> : IFusedIterator<T>
    {
        private T repeatValue;

        /// <summary>
        /// <para>Creates a new iterator that endlessly repeats a single element.</para>
        /// <para>The repeat() function repeats a single value over and over again.</para>
        /// <para>Infinite iterators like repeat() are often used with adapters
        /// like take, in order to make them finite.</para>
        /// <para>If the element type of the iterator you need does not implement Clone,
        /// or if you do not want to keep the repeated element in memory, you can
        /// instead use the repeat_with function.</para>
        /// </summary>
        /// <param name="repeatValue"></param>
        /// <returns></returns>
        internal static Repeat<T> New(T repeatValue)
        {
            Repeat<T> v;
            v.repeatValue = repeatValue;
            return v;
        }

        internal Repeat(T repeatValue)
        {
            this.repeatValue = repeatValue;
        }

        /// <inheritdoc />

        public Option<T> Next()
        {
            return Option<T>.Some(repeatValue);
        }

        /// <inheritdoc />
        public (int, Option<int>) SizeHint()
        {
            return (int.MaxValue, Option<int>.None);
        }
    }
}
