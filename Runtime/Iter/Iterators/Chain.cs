using System.Runtime.CompilerServices;

namespace Koboldgames.Iter
{
    using Koboldgames.Option;

    /// <summary>
    /// <para>An iterator that links two iterators together, in a chain.</para>
    /// <para>This class is created by the Chain method on IIterator. See its documentation for more.</para>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public struct Chain<T> : IIterator<T>
    {
        private IIterator<T> first;
        private IIterator<T> second;


        internal static Chain<T> New(IIterator<T> first, IIterator<T> second)
        {
            Chain<T> v;
            v.first = first;
            v.second = second;
            return v;
        }

        /// <inheritdoc />

        public Option<T> Next()
        {
            Option<T> next = first.Next();
            if (!next.isSome)
            {
                next = second.Next();
                if (!next.isSome)
                {
                    return Option<T>.None;
                }

                return next;
            }

            return next;
        }

        /// <inheritdoc />
        public (int, Option<int>) SizeHint()
        {
            // TODO: This implementation of SizeHint() does not correspond to the one in std::iter::Chain<A, B>
            return (0, Option<int>.None);
        }
    }
}
