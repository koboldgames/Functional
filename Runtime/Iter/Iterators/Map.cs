namespace Koboldgames.Iter
{
    using System;
    using System.Runtime.CompilerServices;
    using Option;

    /// <summary>
    /// <para>An iterator that maps the values of iter with f.</para>
    /// <para>This struct is created by the Map method on IIterator. See its documentation for more.</para>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="U"></typeparam>
    public struct Map<T, U> : IIterator<U>
    {
        private Func<T, U> f;
        private IIterator<T> source;

        /// <summary>
        ///     <para>Takes a closure and creates an iterator which calls that closure on each element.</para>
        ///     <para>Map() transforms one iterator into another, by means of its argument.
        ///     It produces a new iterator which calls this closure on each element of the original iterator.</para>
        ///     <para>If you are good at thinking in types, you can think of Map() like this:
        ///     If you have an iterator that gives you elements of some type A,
        ///     and you want an iterator of some other type B, you can use Map(),
        ///     passing a closure that takes an A and returns a B.</para>
        ///     <para>Map() is conceptually similar to a for loop. However, as Map() is lazy,
        ///     it is best used when you're already working with other iterators.
        ///     If you're doing some sort of looping for a side effect,
        ///     it's considered more idiomatic to use for than Map().</para>
        /// </summary>
        /// <param name="source"></param>
        /// <param name="f"></param>
        /// <returns></returns>

        internal static Map<T, U> New(IIterator<T> source, Func<T, U> f)
        {
            Map<T, U> v;
            v.source = source;
            v.f = f;
            return v;
        }

        /// <inheritdoc />

        public Option<U> Next()
        {
            Option<T> next = source.Next();
            return next.isSome ? Option<U>.Some(f(next.some)) : Option<U>.None;
        }

        /// <inheritdoc />
        public (int, Option<int>) SizeHint()
        {
            return source.SizeHint();
        }
    }
}
