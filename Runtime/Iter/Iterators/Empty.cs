namespace Koboldgames.Iter
{
    using System.Runtime.CompilerServices;
    using Option;

    /// <summary>
    /// <para>An iterator that yields nothing.</para>
    /// <para>This class is created by the empty function. See its documentation for more.</para>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public readonly struct Empty<T> : IExactSizeIterator<T>
    {
        /// <summary>
        /// Creates an iterator that yields nothing.
        /// </summary>
        /// <returns></returns>
        internal static Empty<T> New()
        {
            Empty<T> v;
            return v;
        }

        /// <inheritdoc />

        public int Length() => 0;

        /// <inheritdoc />

        public bool IsEmpty() => true;

        /// <inheritdoc />

        public Option<T> Next() => Option<T>.None;

        /// <inheritdoc />
        public (int, Option<int>) SizeHint() => (0, Option<int>.Some(0));
    }
}
