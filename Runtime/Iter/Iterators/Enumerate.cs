namespace Koboldgames.Iter
{
    using System.Runtime.CompilerServices;
    using Option;

    /// <summary>
    /// <para>An iterator that yields the current count and the element during iteration.</para>
    /// <para>This struct is created by the Enumerate method on IIterator. See its documentation for more.</para>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public struct Enumerate<T> : IIterator<(int, T)>
    {
        private IIterator<T> source;
        private int counter;

        /// <summary>
        ///     <para>Creates an iterator which gives the current iteration count as well as the next value.</para>
        ///     <para>The iterator returned yields pairs (i, val), where i is the current index of
        ///     iteration and val is the value returned by the iterator.</para>
        ///     <para>Enumerate() keeps its count as an int. If you want to count by a differently sized integer,
        ///     the Zip function provides similar functionality.</para>
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>

        internal static Enumerate<T> New(IIterator<T> source)
        {
            Enumerate<T> v;
            v.source = source;
            v.counter = 0;
            return v;
        }

        /// <inheritdoc />

        public Option<(int, T)> Next()
        {
            Option<T> next = source.Next();
            if (!next.isSome)
            {
                return Option<(int, T)>.None;
            }

            var tmp = Option<(int, T)>.Some((counter, next.some));
            counter += 1;
            return tmp;
        }

        /// <inheritdoc />
        public (int, Option<int>) SizeHint()
        {
            return source.SizeHint();
        }
    }
}
