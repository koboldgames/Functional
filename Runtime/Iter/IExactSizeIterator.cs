using System.Runtime.CompilerServices;

namespace Koboldgames.Iter
{
    /// <summary>
    /// <para>An iterator that knows its exact length.</para>
    /// <para>Many Iterators don't know how many times they will iterate, but some do. If an iterator knows how many times it can iterate, providing access to that information can be useful. For example, if you want to iterate backwards, a good start is to know where the end is.</para>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IExactSizeIterator<T> : IFusedIterator<T>
    {
        /// <summary>
        /// <para>Returns the exact length of the iterator.</para>
        /// <para>The implementation ensures that the iterator will return exactly Length more times a Some(T) value, before returning None.</para>
        /// </summary>

        int Length();

        /// <summary>
        /// Returns true if the iterator is empty.
        /// </summary>

        bool IsEmpty();
    }
}
