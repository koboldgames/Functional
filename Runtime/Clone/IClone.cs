using System;

namespace Koboldgames.Clone
{
    /// <summary>
    ///     Provides a deep cloning method for type T
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IClone<out T> : ICloneable
    {
        /// <summary>
        /// Creates a deep clone of type <c>T</c>. You generally want to implement
        /// IClone on the type you want to clone.
        /// But, due to limitations of the C# typesystem,
        /// I cannot refer to the type implementing the interface with generics.
        /// </summary>
        /// <returns></returns>
        T DeepClone();
    }
}
