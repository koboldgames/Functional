namespace Koboldgames.Ops
{
    /// <summary>
    /// Allows containers to return an explicitly immutable reference to a contained value
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IDeref<T>
    {
        /// <summary>
        /// Dereferences the value.
        /// </summary>
        ref readonly T Deref { get; }
    }
}
