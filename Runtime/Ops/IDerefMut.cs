namespace Koboldgames.Ops
{
    /// <summary>
    /// Allows containers to return an explicitly mutable reference to a contained value
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IDerefMut<T> : IDeref<T>
    {
        /// <summary>
        /// Mutably dereferences the value.
        /// </summary>
        ref T DerefMut { get; }
    }
}
