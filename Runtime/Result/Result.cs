namespace Koboldgames.Result
{
    using System;
    using System.Runtime.CompilerServices;
    using Koboldgames.Option;

    /// <summary>
    ///     Encodes the possible states of the result type.
    /// </summary>
    public enum State : byte
    {
        /// <summary>
        /// Represents the result state that contains a valid value
        /// </summary>
        Ok,
        /// <summary>
        /// Represents the result state that contains an error value
        /// </summary>
        Err,
    }

    /// <summary>
    ///     <para>Error handling with the Result type.</para>
    ///     <para><c>Result&lt;T, E&gt;</c> is the type used for returning and
    ///     propagating errors. It is a type with the variants, Ok(T),
    ///     representing success and containing a value, and Err(E), representing
    ///     error and containing an error value.</para>
    /// </summary>
    public struct Result<T, E> : IEquatable<Result<T, E>>
    {
        internal bool isOk;
        internal T ok;
        internal E err;

        /// <summary>
        ///     Create a new result that contains a value.
        /// </summary>




        public static Result<T, E> Ok(in T ok)
        {
            Result<T, E> v;
            v.isOk = true;
            v.ok = ok;
            v.err = default;
            return v;
        }

        /// <summary>
        ///     Creates a new result that contains an error.
        /// </summary>




        public static Result<T, E> Err(in E err)
        {
            Result<T, E> v;
            v.isOk = false;
            v.ok = default;
            v.err = err;
            return v;
        }

        /// <summary>
        ///     Return the state of the result. This simplifies C# pattern matching.
        /// </summary>




        public Result.State State() => isOk ? Result.State.Ok : Result.State.Err;

        /// <summary>
        ///     Returns <c>true</c> if this result is Ok(T).
        /// </summary>




        public bool IsOk() => isOk;

        /// <summary>
        ///     Returns <c>true</c> if this result is Err(E).
        /// </summary>




        public bool IsErr() => !isOk;

        /// <summary>
        ///     <para>Returns an iterator over the possibly contained value.</para>
        ///     <para>The iterator yields one value if the result is Ok(T), otherwise none.</para>
        /// </summary>
        /// <returns></returns>
        public Iter<T> Iter()
        {
            Iter<T> v;
            v.source = Ok();
            return v;
        }

        /// <summary>
        ///     Returns the inner value or throws an exception if the result is not Ok(E).
        /// </summary>
        public T Unwrap() => Expect("Called Result.Unwrap() on an Err value");

        /// <summary>
        ///     Returns the error value or throws an exception if the result is not Err(E).
        /// </summary>
        public E UnwrapErr() => ExpectErr("Called Result.UnwrapErr() on an Ok value");

        /// <summary>
        /// <para>Returns the contained Ok value or a provided default.</para>
        /// <para>Arguments passed to UnwrapOr are eagerly evaluated; if you are passing the result of a function call, it is recommended to use UnwrapOrElse, which is lazily evaluated.</para>
        /// </summary>
        /// <param name="fallback"></param>
        /// <returns></returns>




        public T UnwrapOr(T fallback) => isOk ? ok : fallback;

        /// <summary>
        /// Returns the contained Ok value or computes it from a closure.
        /// </summary>
        /// <param name="f"></param>
        /// <returns></returns>




        public T UnwrapOrElse(Func<T> f) => isOk ? ok : f();

        /// <summary>
        ///     Returns the inner value or throws an exception with a custom message.
        /// </summary>
        public T Expect(in string msg)
        {
            return isOk ? ok : throw new UnwrapException($"{msg}: {err}");
        }

        /// <summary>
        ///     Returns the error value or throws an exception with a custom message.
        /// </summary>
        public E ExpectErr(in string msg)
        {
            return !isOk ? err : throw new UnwrapException($"{msg}: {ok}");
        }

        /// <summary>
        ///     Converts the result to an option type, discarding the error.
        /// </summary>




        public Option<T> Ok()
        {
            return isOk ? Option<T>.Some(ok) : Option<T>.None;
        }

        /// <summary>
        ///     Converts the result to an option type, discarding the value.
        /// </summary>




        public Option<E> Err()
        {
            return !isOk ? Option<E>.Some(err) : Option<E>.None;
        }

        /// <summary>
        ///     Returns other if the result is Ok, otherwise returns the Err value of self.
        /// </summary>




        public Result<U, E> And<U>(in Result<U, E> other)
        {
            return isOk ? other : Result<U, E>.Err(err);
        }

        /// <summary>
        ///     Calls op if the result is Ok, otherwise returns the Err value of self.
        ///     This function can be used for control flow based on Result values.
        /// </summary>




        public Result<U, E> AndThen<U>(Func<T, Result<U, E>> op)
        {
            return isOk ? op(ok) : Result<U, E>.Err(err);
        }

        /// <summary>
        ///     Maps a <c>Result&lt;T, E&gt;</c> to <c>Result&lt;U, E&gt;</c> by applying a function to a
        ///     contained Ok value, leaving an Err value untouched.
        ///     This function can be used to compose the results of two functions.
        /// </summary>




        public Result<U, E> Map<U>(Func<T, U> op)
        {
            return isOk ? Result<U, E>.Ok(op(ok)) : Result<U, E>.Err(err);
        }

        /// <summary>
        ///     Maps a <c>Result&lt;T, E&gt;</c> to <c>Result&lt;T, F&gt;</c> by applying a function to a
        ///     contained Err value, leaving an Ok value untouched.
        ///     This function can be used to pass through a successful result while
        ///     handling an error.
        /// </summary>




        public Result<T, F> MapErr<F>(Func<E, F> op)
        {
            return isOk ? Result<T, F>.Ok(ok) : Result<T, F>.Err(op(err));
        }

        /// <summary>
        ///     Applies a function to the contained value (if any), or returns the provided default (if not).
        /// </summary>




        public U MapOr<U>(in U fallback, Func<T, U> op)
        {
            return isOk ? op(ok) : fallback;
        }

        /// <summary>
        ///     Maps a <c>Result&lt;T, E&gt;</c> to U by applying a function to a contained Ok value, or a fallback function to a
        ///     contained Err value.
        ///     This function can be used to unpack a successful result while handling an error.
        /// </summary>




        public U MapOrElse<U>(Func<E, U> fallback, Func<T, U> op)
        {
            return isOk ? op(ok) : fallback(err);
        }

        /// <summary>
        ///     Returns other if the result is Err, otherwise returns the Ok value of self.
        ///     Arguments passed to or are eagerly evaluated; if you are passing
        ///     the result of a function call, it is recommended to use or_else,
        ///     which is lazily evaluated.
        /// </summary>




        public Result<T, F> Or<F>(in Result<T, F> other)
        {
            return isOk ? Result<T, F>.Ok(ok) : other;
        }

        /// <summary>
        ///     Calls op if the result is Err, otherwise returns the Ok value of self.
        ///     This function can be used for control flow based on result values.
        /// </summary>




        public Result<T, F> OrElse<F>(Func<E, Result<T, F>> op)
        {
            return isOk ? Result<T, F>.Ok(ok) : op(err);
        }

        /// <inheritdoc />
        public bool Equals(Result<T, E> other)
        {
            return (!isOk && !other.isOk && Equals(err, other.err)) || (isOk && other.isOk && Equals(ok, other.ok));
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            return (obj != null) && (obj is Result<T, E>) && Equals((Result<T, E>) obj);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            return isOk ? 17 * ok.GetHashCode() + 23 : 17 * err.GetHashCode() + 23;
        }

        /// <summary>
        /// Compares two results for equality
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator ==(Result<T, E> left, Result<T, E> right)
        {
            return Equals(left, right);
        }

        /// <summary>
        /// Compares two results for inequality
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator !=(Result<T, E> left, Result<T, E> right)
        {
            return !Equals(left, right);
        }

        /// <summary>
        ///     Returns a string representation of the result for debugging purposes.
        /// </summary>
        public override string ToString()
        {
            return isOk ? $"Ok({ok})" : $"Err({err})";
        }
    }
}
