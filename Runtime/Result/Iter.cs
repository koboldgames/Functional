namespace Koboldgames.Result
{
    using System.Runtime.CompilerServices;
    using Koboldgames.Iter;
    using Koboldgames.Option;

    /// <summary>
    /// An iterator over the single value of type <c>T</c> in <c>Result&lt;T, E&gt;</c>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public struct Iter<T> : IExactSizeIterator<T>
    {
        internal Option<T> source;

        /// <inheritdoc />

        public int Length() => source.isSome ? 1 : 0;

        /// <inheritdoc />

        public bool IsEmpty() => !source.isSome;

        /// <inheritdoc />

        public Option<T> Next()
        {
            if (!source.isSome)
            {
                return Option<T>.None;
            }

            Option<T> tmp = source;
            source = Option<T>.None;
            return tmp;
        }

        /// <inheritdoc />
        public (int, Option<int>) SizeHint()
        {
            int count = source.isSome ? 1 : 0;
            return (count, Option<int>.Some(count));
        }
    }
}
