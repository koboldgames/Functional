namespace Koboldgames.Option
{
    using System;
    using System.Runtime.CompilerServices;
    using Result;

    /// <summary>
    ///     Encodes the possible states of the option type.
    /// </summary>
    public enum State : byte
    {
        /// <summary>
        /// Represents the option state that contains a value
        /// </summary>
        Some,
        /// <summary>
        /// Represents the option state that does not contain a value
        /// </summary>
        None,
    }

    /// <summary>
    ///     Type Option represents an optional value: every Option is either Some
    ///     and contains a value, or None, and does not. Option types have a number
    ///     of uses:
    ///     <list type="bullet">
    ///         <item>
    ///             <description>Initial values</description>
    ///         </item>
    ///         <item>
    ///             <description>
    ///                 Return values for functions that are not defined over their entire input range (partial
    ///                 functions)
    ///             </description>
    ///         </item>
    ///         <item>
    ///             <description>Return value for otherwise reporting simple errors, where None is returned on error</description>
    ///         </item>
    ///         <item>
    ///             <description>Optional struct fields</description>
    ///         </item>
    ///         <item>
    ///             <description>Struct fields that can be loaned or "taken"</description>
    ///         </item>
    ///         <item>
    ///             <description>Optional function arguments</description>
    ///         </item>
    ///         <item>
    ///             <description>Nullable pointers</description>
    ///         </item>
    ///         <item>
    ///             <description>Swapping things out of difficult situations</description>
    ///         </item>
    ///     </list>
    /// </summary>
    public struct Option<T> : IEquatable<Option<T>>, IComparable<Option<T>>, IComparable
    {
        internal bool isSome;
        internal T some;

        /// <summary>
        ///     Create a new option that contains a value
        /// </summary>
        /// <param name="some">A value to be contained in the option</param>
        /// <returns>A new option instance</returns>




        public static Option<T> Some(in T some)
        {
            Option<T> v;
            v.isSome = true;
            v.some = some;
            return v;
        }

        /// <summary>
        ///     A static None value.
        /// </summary>
        public static readonly Option<T> None = new Option<T>
        {
            isSome = false,
            some = default,
        };

        /// <summary>
        ///     Return the state of the option. This simplifies C# pattern matching.
        /// </summary>




        public Koboldgames.Option.State State() => isSome ? Koboldgames.Option.State.Some : Koboldgames.Option.State.None;

        /// <summary>
        ///     Returns <c>true</c> if this option is <c>Some(T)</c>.
        /// </summary>




        public bool IsSome() => isSome;

        /// <summary>
        ///     Returns <c>true</c> if this option is <c>None</c>.
        /// </summary>




        public bool IsNone() => !isSome;

        /// <summary>
        ///     Returns an iterator over the possibly contained value.
        ///     The iterator yields one value if the result is Some(T), otherwise none.
        /// </summary>
        /// <returns></returns>
        public Iter<T> Iter()
        {
            Iter<T> v;
            v.source = this;
            return v;
        }

        /// <summary>
        ///     Unwraps the inner value of the option.
        /// </summary>
        /// <returns>The inner value of the option</returns>
        /// <exception cref="UnwrapException">If the option is <c>None</c></exception>
        public T Unwrap() => Expect("Called Option.Unwrap() on a None value");

        /// <summary>
        /// <para>Returns the contained Some value or a provided default.</para>
        /// <para>Arguments passed to UnwrapOr are eagerly evaluated; if you are passing the result of a function call, it is recommended to use UnwrapOrElse, which is lazily evaluated.</para>
        /// </summary>
        /// <param name="fallback"></param>
        /// <returns></returns>




        public T UnwrapOr(in T fallback) => isSome ? some : fallback;

        /// <summary>
        /// Returns the contained Some value or computes it from a closure.
        /// </summary>
        /// <param name="f"></param>
        /// <returns></returns>




        public T UnwrapOrElse(Func<T> f) => isSome ? some : f();

        /// <summary>
        ///     Unwraps the inner value of the option or fails with a custom message.
        /// </summary>
        /// <param name="msg">A custom failure message</param>
        /// <returns>The inner value of the option</returns>
        /// <exception cref="UnwrapException">If the option is <c>None</c></exception>
        public T Expect(in string msg)
        {
            return isSome ? some : throw new UnwrapException(msg);
        }

        /// <summary>
        ///     Converts the option to a result with the specified error value, if
        ///     the option was None.
        /// </summary>
        /// <param name="err">An error assigned if the option is <c>None</c></param>
        /// <typeparam name="E">The error type</typeparam>
        /// <returns>A result that contains either the option value or an error</returns>




        public Result<T, E> OkOr<E>(in E err)
        {
            return isSome ? Result<T, E>.Ok(some) : Result<T, E>.Err(err);
        }

        /// <summary>
        ///     Converts the option to a result with the specified error value, if
        ///     the option was None. The error closure is evaluated lazily, so only
        ///     when the option is None.
        /// </summary>
        /// <param name="op">A function that produces an error value if the option is None.</param>
        /// <typeparam name="E">The error type</typeparam>
        /// <returns>A result that contains either the option value or an error</returns>




        public Result<T, E> OkOrElse<E>(Func<E> op)
        {
            return isSome ? Result<T, E>.Ok(some) : Result<T, E>.Err(op());
        }

        /// <summary>
        ///     Returns None if the option is None, otherwise calls predicate with
        ///     the wrapped value and returns:
        ///     Some(T) if predicate returns true (where T is the wrapped value), and
        ///     None if predicate returns false.
        /// </summary>
        /// <param name="predicate">
        ///     A function that returns a boolean value based on the option value. Called only if the option is
        ///     Some(T).
        /// </param>
        /// <returns>An option that is either Some(T) or None depending on the return value of the predicate</returns>




        public Option<T> Filter(Func<T, bool> predicate)
        {
            return (isSome && predicate(some)) ? Some(some) : None;
        }

        /// <summary>
        ///     Returns None if the option is None, otherwise returns other.
        /// </summary>
        /// <param name="other">An additional option</param>
        /// <typeparam name="U">The type of the value contained in the resulting option</typeparam>
        /// <returns>Some(U) if other is Some(U) or None if either of the two options is None</returns>




        public Option<U> And<U>(in Option<U> other)
        {
            return isSome ? other : Option<U>.None;
        }

        /// <summary>
        ///     Returns None if the option is None, otherwise calls f with the wrapped value and returns the result.
        ///     Some languages call this operation flatmap.
        /// </summary>
        /// <param name="op">A function that returns a new option</param>
        /// <typeparam name="U">The type of the value contained in the resulting option</typeparam>
        /// <returns>Some(U) if the result of op is Some(U) or None if either of the two options is None</returns>




        public Option<U> AndThen<U>(Func<T, Option<U>> op)
        {
            return isSome ? op(some) : Option<U>.None;
        }

        /// <summary>
        ///     Maps an <c>Option&lt;T&gt;</c> to <c>Option&lt;U&gt;</c> by applying a function to a contained value.
        /// </summary>
        /// <param name="op">A function that transforms the value of type <c>T</c> to type <c>U</c></param>
        /// <typeparam name="U">The type of the value contained in the resulting option</typeparam>
        /// <returns>A transformed option</returns>




        public Option<U> Map<U>(Func<T, U> op)
        {
            return isSome ? Option<U>.Some(op(some)) : Option<U>.None;
        }

        /// <summary>
        ///     Applies a function to the contained value (if any), or returns the provided default (if not).
        /// </summary>
        /// <param name="fallback">A value returned if the option is <c>None</c></param>
        /// <param name="op">A function that transforms the value of type <c>T</c> to type <c>U</c></param>
        /// <typeparam name="U">The type of the value contained in the resulting option</typeparam>
        /// <returns>A value of type <c>U</c></returns>




        public U MapOr<U>(in U fallback, Func<T, U> op)
        {
            return isSome ? op(some) : fallback;
        }

        /// <summary>
        ///     Applies a function to the contained value (if any), or computes a default (if not).
        /// </summary>
        /// <param name="fallback">A function that returns a value of type <c>U</c> if the option is None</param>
        /// <param name="op">A function that transforms the value of type <c>T</c> to type <c>U</c></param>
        /// <typeparam name="U">The type of the value contained in the resulting option</typeparam>
        /// <returns>A value of type <c>U</c></returns>




        public U MapOrElse<U>(Func<U> fallback, Func<T, U> op)
        {
            return isSome ? op(some) : fallback();
        }

        /// <summary>
        ///     Returns the option if it contains a value, otherwise returns other.
        ///     Arguments passed to or are eagerly evaluated; if you are passing
        ///     the result of a function call, it is recommended to use or_else,
        ///     which is lazily evaluated.
        /// </summary>
        /// <param name="other">The option returned if the current option is None</param>
        /// <returns>The union of two options</returns>




        public Option<T> Or(in Option<T> other)
        {
            return isSome ? this : other;
        }

        /// <summary>
        ///     Returns the option if it contains a value, otherwise calls op and returns the result.
        /// </summary>
        /// <param name="op">A function that returns a second option</param>
        /// <returns>The union of two options</returns>




        public Option<T> OrElse(Func<Option<T>> op)
        {
            return isSome ? this : op();
        }

        /// <inheritdoc />
        public bool Equals(Option<T> other)
        {
            return !(isSome || other.isSome) || (isSome && other.isSome && Equals(some, other.some));
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            return (obj != null) && (obj is Option<T>) && Equals((Option<T>) obj);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            return isSome ? 17 * some.GetHashCode() + 23 : 0;
        }

        /// <inheritdoc />
        public int CompareTo(Option<T> other)
        {
            return isSome.CompareTo(other.isSome);
        }

        /// <inheritdoc />
        public int CompareTo(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return 1;
            }

            return obj is Option<T> other
                ? CompareTo(other)
                : throw new ArgumentException($"Object must be of type {nameof(Option<T>)}");
        }

        /// <summary>
        /// Assesses whether the left-hand option is lesser the right-hand option
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator <(Option<T> left, Option<T> right)
        {
            return left.CompareTo(right) < 0;
        }

        /// <summary>
        /// Assesses whether the left-hand option is greater the right-hand option
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator >(Option<T> left, Option<T> right)
        {
            return left.CompareTo(right) > 0;
        }

        /// <summary>
        /// Assesses whether the left-hand option is lesser-equal the right-hand option
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator <=(Option<T> left, Option<T> right)
        {
            return left.CompareTo(right) <= 0;
        }

        /// <summary>
        /// Assesses whether the left-hand option is greater-equal the right-hand option
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator >=(Option<T> left, Option<T> right)
        {
            return left.CompareTo(right) >= 0;
        }

        /// <summary>
        /// Compares two options for equality
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator ==(Option<T> left, Option<T> right)
        {
            return Equals(left, right);
        }

        /// <summary>
        /// Compares two options for inequality
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator !=(Option<T> left, Option<T> right)
        {
            return !Equals(left, right);
        }

        /// <summary>
        /// Implicitly convert a value to an optional value
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static implicit operator Option<T>(T val)
        {
            return val == null ? None : Some(val);
        }

        /// <summary>
        ///     Returns a string representation of the option for debugging purposes.
        /// </summary>
        public override string ToString()
        {
            return isSome ? $"Some({some})" : "None";
        }
    }
}
