using System;

namespace Koboldgames.Option
{
    /// <summary>
    ///     Thrown when either an <c>Option&lt;T&gt;</c> or <c>Result&lt;T, E&gt;</c> cannot be unwrapped into the values they
    ///     contain.
    /// </summary>
    public sealed class UnwrapException : Exception
    {
        /// <summary>
        /// Creates a new unwrap exception without a message
        /// </summary>
        public UnwrapException()
        {
        }

        /// <summary>
        /// Creates a new unwrap exception with a custom message
        /// </summary>
        /// <param name="msg"></param>
        public UnwrapException(string msg) : base(msg)
        {
        }
    }
}
