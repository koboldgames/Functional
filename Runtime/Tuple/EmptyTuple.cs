using System;

namespace Koboldgames.Tuple
{
    /// <summary>
    /// The C# standard library does not provide an empty tuple.
    /// An empty tuple is a type that contains no data and only a single value.
    /// It is similar to <c>null</c>, with the notable difference that it is typesafe.
    /// </summary>
    public readonly struct EmptyTuple : IEquatable<EmptyTuple>
    {
        /// <summary>
        /// Refers to the single static instance of the empty tuple
        /// </summary>
        public static readonly EmptyTuple Value = new EmptyTuple();

        /// <summary>
        /// Always returns true
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(EmptyTuple other)
        {
            return true;
        }

        /// <summary>
        /// Returns string representation of the empty tuple for debugging purposes
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "()";
        }

        /// <summary>
        /// Returns true if the comparing type is also <c>EmptyTuple</c>, false otherwise
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            return obj is EmptyTuple;
        }

        /// <summary>
        /// Always returns the constant hash code <c>1</c>
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return 1;
        }

        /// <summary>
        /// Always returns true
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator ==(EmptyTuple left, EmptyTuple right)
        {
            return true;
        }

        /// <summary>
        /// Always returns false
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator !=(EmptyTuple left, EmptyTuple right)
        {
            return false;
        }

    }
}
