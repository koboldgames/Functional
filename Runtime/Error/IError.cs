using System;
using Koboldgames.Option;

namespace Koboldgames.Error
{
    /// <summary>
    ///     <c>IError</c> is an interface representing the basic expectations for error values,
    ///     i.e., values of type E in <c>Result&lt;T, E&gt;</c>. Errors must describe themselves
    ///     through the <c>IFormattable</c> interface, and may provide cause chain information.
    ///     The Source method is generally used when errors cross "abstraction boundaries".
    ///     If one module must report an error that is caused by an error from a
    ///     lower-level module, it can allow access to that error via the Source method.
    ///     This makes it possible for the high-level module to provide its own
    ///     errors while also revealing some of the implementation for debugging via
    ///     Source chains.
    /// </summary>
    public interface IError : IFormattable
    {
        /// <summary>
        ///     The lower-level source of this error, if any.
        /// </summary>
        Option<IError> Source();
    }
}
