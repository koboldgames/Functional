namespace Koboldgames.Tests
{
    using Option;
    using Result;
    using NUnit.Framework;

    [TestFixture]
    public class OptionTests
    {
        [Test]
        public void And()
        {
            Option<int> optA = Option<int>
                .Some(100)
                .And(Option<int>.Some(200));

            Assert.IsTrue(optA.Unwrap() == 200);

            Option<int> optB = Option<int>
                .None
                .And(Option<int>.Some(200));

            Assert.IsTrue(optB.IsNone());
            Option<int> optC = Option<int>
                .Some(100)
                .And(Option<int>.None);

            Assert.IsTrue(optC.IsNone());
        }

        [Test]
        public void AndThen()
        {
            Option<int> optA = Option<int>
                .Some(100)
                .AndThen(i => Option<int>.Some(i + 50));

            Assert.IsTrue(optA.Unwrap() == 150);

            Option<int> optB = Option<int>
                .None
                .AndThen(i => Option<int>.Some(i + 50));

            Assert.IsTrue(optB.IsNone());

            Option<int> optC = Option<int>
                .Some(100)
                .AndThen(i => Option<int>.None);

            Assert.IsTrue(optC.IsNone());
        }

        [Test]
        public void Expect()
        {
            var optA = Option<int>.Some(100);
            Assert.IsTrue(optA.Expect(string.Empty) == 100);

            var optB = Option<int>.None;
            Assert.Catch<Option.UnwrapException>(() => optB.Expect(string.Empty));
        }

        [Test]
        public void Filter()
        {
            Option<int> optA = Option<int>
                .Some(100)
                .Filter(i => i == 100);

            Assert.IsTrue(optA.IsSome());

            Option<int> optB = Option<int>
                .Some(101)
                .Filter(i => i == 100);

            Assert.IsTrue(optB.IsNone());
        }

        [Test]
        public void IsNone()
        {
            var optA = Option<int>.Some(100);
            Assert.IsFalse(optA.IsNone());

            var optB = Option<int>.None;
            Assert.IsTrue(optB.IsNone());
        }

        [Test]
        public void IsSome()
        {
            var optA = Option<int>.Some(100);
            Assert.IsTrue(optA.IsSome());

            var optB = Option<int>.None;
            Assert.IsFalse(optB.IsSome());
        }

        [Test]
        public void Map()
        {
            Option<int> optA = Option<int>
                .Some(100)
                .Map(i => i + 50);

            Assert.IsTrue(optA.Unwrap() == 150);

            Option<int> optB = Option<int>
                .None
                .Map(i => i + 50);

            Assert.IsTrue(optB.IsNone());
        }

        [Test]
        public void MapOr()
        {
            int contentA = Option<int>
                .Some(100)
                .MapOr(0, i => i + 50);

            Assert.IsTrue(contentA == 150);

            int contentB = Option<int>
                .None
                .MapOr(0, i => i + 50);

            Assert.IsTrue(contentB == 0);
        }

        [Test]
        public void MapOrElse()
        {
            int contentA = Option<int>
                .Some(100)
                .MapOrElse(() => 0, i => i + 50);

            Assert.IsTrue(contentA == 150);

            int contentB = Option<int>
                .None
                .MapOrElse(() => 0, i => i + 50);

            Assert.IsTrue(contentB == 0);
        }

        [Test]
        public void OkOr()
        {
            var optA = Option<int>.Some(100);
            Result<int, string> unused = optA.OkOr(string.Empty);

            var optB = Option<int>.None;
            Result<int, string> unused1 = optB.OkOr(string.Empty);
        }

        [Test]
        public void OkOrElse()
        {
            var optA = Option<int>.Some(100);
            Result<int, string> unused = optA.OkOrElse(() => string.Empty);

            var optB = Option<int>.None;
            Result<int, string> unused1 = optB.OkOrElse(() => string.Empty);
        }

        [Test]
        public void Or()
        {
            Option<int> optA = Option<int>
                .Some(100)
                .Or(Option<int>.Some(200));

            Assert.IsTrue(optA.Unwrap() == 100);
            Option<int> optB = Option<int>
                .None
                .Or(Option<int>.Some(200));

            Assert.IsTrue(optB.Unwrap() == 200);
            Option<int> optC = Option<int>
                .Some(100)
                .Or(Option<int>.None);

            Assert.IsTrue(optC.Unwrap() == 100);

            Option<int> optD = Option<int>
                .None
                .Or(Option<int>.None);

            Assert.IsTrue(optD.IsNone());
        }

        [Test]
        public void OrElse()
        {
            Option<int> optA = Option<int>
                .Some(100)
                .OrElse(() => Option<int>.Some(200));

            Assert.IsTrue(optA.Unwrap() == 100);
            Option<int> optB = Option<int>
                .None
                .OrElse(() => Option<int>.Some(200));

            Assert.IsTrue(optB.Unwrap() == 200);
            Option<int> optC = Option<int>
                .Some(100)
                .OrElse(() => Option<int>.None);

            Assert.IsTrue(optC.Unwrap() == 100);
            Option<int> optD = Option<int>
                .None
                .OrElse(() => Option<int>.None);

            Assert.IsTrue(optD.IsNone());
        }

        [Test]
        public void State()
        {
            var optA = Option<int>.Some(100);
            Assert.IsTrue(optA.State() == Option.State.Some);

            var optB = Option<int>.None;
            Assert.IsTrue(optB.State() == Option.State.None);
        }

        [Test]
        public void Unwrap()
        {
            var optA = Option<int>.Some(100);
            Assert.IsTrue(optA.Unwrap() == 100);

            var optB = Option<int>.None;
            Assert.Catch<Option.UnwrapException>(() => optB.Unwrap());
        }
    }
}
