namespace Koboldgames.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Iter;
    using Option;
    using NUnit.Framework;

    [TestFixture]
    public class IteratorTests
    {
        [Test]
        public void Empty()
        {
            Empty<int> iterA = Iterator.Empty<int>();

            Assert.That(iterA.Length(), Is.EqualTo(0));
            Assert.That(iterA.IsEmpty(), Is.EqualTo(true));
            Assert.That(iterA.Collect(), Is.EqualTo(new int[0]));
        }

        [Test]
        public void Once()
        {
            Once<int> iterA = Iterator.Once(100);

            Assert.That(iterA.Length(), Is.EqualTo(1));
            Assert.That(iterA.IsEmpty(), Is.EqualTo(false));
            Assert.That(iterA.Collect(), Is.EqualTo(new int[] { 100 }));
        }

        [Test]
        public void Repeat()
        {
            Repeat<int> iterA = Iterator.Repeat(100);
            Assert.That(iterA.Next(), Is.EqualTo(Option<int>.Some(100)));
            Assert.That(iterA.Next(), Is.EqualTo(Option<int>.Some(100)));
            Assert.That(iterA.Next(), Is.EqualTo(Option<int>.Some(100)));
            Assert.That(iterA.Next(), Is.EqualTo(Option<int>.Some(100)));
            Assert.That(iterA.Next(), Is.EqualTo(Option<int>.Some(100)));
            Assert.That(iterA.Next(), Is.EqualTo(Option<int>.Some(100)));
        }

        [Test]
        public void Range()
        {
            Range iterA = Iterator.Range(0, 3);
            Assert.That(iterA.Next(), Is.EqualTo(Option<int>.Some(0)));
            Assert.That(iterA.Next(), Is.EqualTo(Option<int>.Some(1)));
            Assert.That(iterA.Next(), Is.EqualTo(Option<int>.Some(2)));
            Assert.That(iterA.Next().IsNone(), Is.True);

            Range iterE = Iterator.Range(4, 0);
            Assert.That(iterE.Next().IsNone(), Is.True);
        }

        [Test]
        public void FlipFlop()
        {
            FlipFlop<int> iterA = Iterator.FlipFlop(100);
            Assert.That(iterA.Next(), Is.EqualTo(Option<int>.Some(100)));
            Assert.That(iterA.Next(), Is.EqualTo(Option<int>.None));
            Assert.That(iterA.Next(), Is.EqualTo(Option<int>.Some(100)));
            Assert.That(iterA.Next(), Is.EqualTo(Option<int>.None));
            Assert.That(iterA.Next(), Is.EqualTo(Option<int>.Some(100)));
            Assert.That(iterA.Next(), Is.EqualTo(Option<int>.None));
        }

        [Test]
        public void FromFunc()
        {
            FromFunc<int> iterA = Iterator.FromFunc(() => Option<int>.Some(100));
            Assert.That(iterA.Next(), Is.EqualTo(Option<int>.Some(100)));

            FromFunc<int> iterB = Iterator.FromFunc(() => Option<int>.None);
            Assert.That(iterB.Next().IsNone(), Is.True);
        }

        [Test]
        public void Take()
        {
            ReadOnlyCollection<int> resA = Iterator.Repeat(100)
                .Take(3)
                .Collect();

            Assert.That(resA, Is.EqualTo(new int[] {100, 100, 100}));

            ReadOnlyCollection<int> resB = Iterator.Repeat(100)
                .Take(0)
                .Collect();

            Assert.That(resB, Is.EqualTo(new int[0]));

            ReadOnlyCollection<int> resC = Iterator.Once(100)
                .Take(3)
                .Collect();

            Assert.That(resC, Is.EqualTo(new int[] {100}));
        }

        [Test]
        public void ForEachLoopInterop()
        {
            IteratorAdaptor<int> iter = Iterator.Range(0, 10).Adapt();
            foreach (int i in iter)
            {
                Console.WriteLine($"{i}");
            }
        }

        [Test]
        public void EnumerableInterop()
        {
            IEnumerable<string> dataA = System.Linq.Enumerable.Repeat(string.Empty, 2);
            ReadOnlyCollection<string> resA = dataA
                .Iter()
                .Collect();

            Assert.That(resA, Is.EquivalentTo(new[] {string.Empty, string.Empty}));

            List<int> dataB = new List<int> {0, 1};
            int[] resB = dataB
                .Iter()
                .Collect();

            Assert.That(resB, Is.EquivalentTo(new[] {0, 1}));

            int[] dataC = {0, 1};
            int[] resC = dataC
                .Iter()
                .Collect();

            Assert.That(resC, Is.EquivalentTo(new[] {0, 1}));

            (int, int)[] dataD = {(0, 0), (1, 1)};
            Dictionary<int, int> unused = dataD
                .Iter()
                .Collect<int, int, Dictionary<int, int>>();
        }

        [Test]
        public void Collect()
        {
            int[] resD = Iterator.Once(1)
                .Collect();
            Assert.That(resD, Has.Length.EqualTo(1));

            List<int> resB = Iterator.Once(1)
                .Collect<int, List<int>>();
            Assert.That(resB, Has.Count.EqualTo(1));

            Dictionary<int, int> resC = Iterator.Once((0, 1))
                .Collect<int, int, Dictionary<int, int>>();
            Assert.That(resC, Has.Count.EqualTo(1));

            int[] resA = Iterator.Once(1)
                .Collect();
            Assert.That(resA, Has.Length.EqualTo(1));
        }

        [Test]
        public void All()
        {
            int[] dataA = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
            Assert.That(dataA.Iter().All(e => e < 10), Is.True);
            Assert.That(dataA.Iter().All(e => e < 9), Is.False);
            Assert.That(dataA.Iter().All(e => e < 1), Is.False);

            int[] dataB = {};
            Assert.That(dataB.Iter().All(e => e < 10), Is.True);
            Assert.That(dataB.Iter().All(e => e < 9), Is.True);
            Assert.That(dataB.Iter().All(e => e < 1), Is.True);
        }

        [Test]
        public void Any()
        {
            int[] dataA = new[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
            Assert.That(dataA.Iter().Any(e => e < 10), Is.True);
            Assert.That(dataA.Iter().Any(e => e < 9), Is.True);
            Assert.That(dataA.Iter().Any(e => e < 0), Is.False);

            var dataB = new int[0];
            Assert.That(dataB.Iter().Any(e => e < 10), Is.False);
            Assert.That(dataB.Iter().Any(e => e < 9), Is.False);
            Assert.That(dataB.Iter().Any(e => e < 0), Is.False);
        }

        [Test]
        public void Chain()
        {
            int[] dataA = new[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
            int[] dataB = new[] {10, 11, 12, 13, 14};
            var dataC = new int[0];

            Assert.That(
                dataA.Iter().Chain(dataB.Iter()).Collect(),
                Is.EquivalentTo(new[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14})
            );
            Assert.That(
                dataA.Iter().Chain(dataC.Iter()).Collect(),
                Is.EquivalentTo(new[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9})
            );
        }

        [Test]
        public void Count()
        {
            int[] dataA = new[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
            Assert.That(dataA.Iter().Count(), Is.EqualTo(10));

            var dataB = new int[0];
            Assert.That(dataB.Iter().Count(), Is.EqualTo(0));
        }

        [Test]
        public void Enumerate()
        {
            int[] dataA = new[] {10, 11, 12, 13, 14};
            Assert.That(
                dataA.Iter().Enumerate().Collect(),
                Is.EquivalentTo(new[] {(0, 10), (1, 11), (2, 12), (3, 13), (4, 14)})
            );

            var dataB = new int[0];
            Assert.That(
                dataB.Iter().Enumerate().Collect(),
                Is.EquivalentTo(new (int, int)[0])
            );
        }

        [Test]
        public void Filter()
        {
            int[] dataA = new[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
            Assert.That(
                dataA.Iter().Filter(e => e < 5).Collect(),
                Is.EquivalentTo(new[] {0, 1, 2, 3, 4})
            );

            var dataB = new int[0];
            Assert.That(
                dataB.Iter().Filter(e => e < 5).Collect(),
                Is.EquivalentTo(new int[0])
            );
        }

        [Test]
        public void TryFold()
        {
            throw new NotImplementedException();
        }

        [Test]
        public void Fold()
        {
            Assert.That(Iterator.Range(1, 5).Fold(0, (s, e) => s + e), Is.EqualTo(10));
            Assert.That(Iterator.Empty<int>().Fold(0, (s, e) => s + e), Is.EqualTo(0));
        }

        [Test]
        public void ForEach()
        {
            int[] dataA = new[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
            var counter = 0;
            dataA.Iter().ForEach(e => counter += 1);
            Assert.That(counter, Is.EqualTo(10));

            int[] dataB = new int[0];
            counter = 0;
            dataB.Iter().ForEach(e => counter += 1);
            Assert.That(counter, Is.EqualTo(0));
        }

        [Test]
        public void StepBy()
        {
            int[] dataA = new[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
            Assert.That(
                dataA.Iter().StepBy(2).Collect(),
                Is.EquivalentTo(new[] {0, 2, 4, 6, 8})
            );

            int[] dataB = new int[0];
            Assert.That(
                dataB.Iter().StepBy(2).Count(),
                Is.EqualTo(0)
            );

            int[] dataC = new[] {0, 1, 2};
            Assert.That(
                dataC.Iter().StepBy(4).Collect(),
                Is.EquivalentTo(new[] {0})
            );
        }

        [Test]
        public void TryForEach()
        {
            throw new NotImplementedException();
        }

        [Test]
        public void Find()
        {
            int[] dataA = new[] {0, 1, 2, 3, 3, 4, 5, 4};
            Assert.That(
                dataA.Iter().Enumerate().Find(e => e.Item2 == 0),
                Is.EqualTo(Option<(int, int)>.Some((0, 0)))
            );
            Assert.That(
                dataA.Iter().Enumerate().Find(e => e.Item2 == 3),
                Is.EqualTo(Option<(int, int)>.Some((3, 3)))
            );
            Assert.That(
                dataA.Iter().Enumerate().Find(e => e.Item2 == 4),
                Is.EqualTo(Option<(int, int)>.Some((5, 4)))
            );

            int[] dataB = new int[0];
            Assert.That(
                dataB.Iter().Find(e => true),
                Is.EqualTo(Option<int>.None)
            );
        }

        [Test]
        public void Position()
        {
            char[] dataA = new[] {'0', '1', '2', '3', '4', '5'};
            Assert.That(
                dataA.Iter().Position(e => e == '2'),
                Is.EqualTo(Option<int>.Some(2))
            );
            Assert.That(
                dataA.Iter().Position(e => e == '9'),
                Is.EqualTo(Option<int>.None)
            );

            int[] dataB = new int[0];
            Assert.That(
                dataB.Iter().Position(e => e == '0'),
                Is.EqualTo(Option<int>.None)
            );
        }

        [Test]
        public void Hash()
        {
            int[] dataA = new int[0];
            Assert.That(
                dataA.Iter().Hash(),
                Is.EqualTo(1)
            );

            int[] dataB = new[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
            Assert.That(
                dataB.Iter().Hash(),
                Is.EqualTo(669445632)
            );

            int[] dataC = new[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
            Assert.That(
                dataC.Iter().Hash(),
                Is.EqualTo(-1812837036)
            );
        }

        [Test]
        public void Fuse()
        {
            Fuse<int> iterB = Iterator.FlipFlop(100)
                .Fuse();
            Assert.That(iterB.Next(), Is.EqualTo(Option<int>.Some(100)));
            Assert.That(iterB.Next(), Is.EqualTo(Option<int>.None));
            Assert.That(iterB.Next(), Is.EqualTo(Option<int>.None));
            Assert.That(iterB.Next(), Is.EqualTo(Option<int>.None));
            Assert.That(iterB.Next(), Is.EqualTo(Option<int>.None));
            Assert.That(iterB.Next(), Is.EqualTo(Option<int>.None));
        }

        [Test]
        public void Last()
        {
            int[] dataA = new[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
            Assert.That(dataA.Iter().Last(), Is.EqualTo(Option<int>.Some(9)));

            var dataB = new int[0];
            Assert.That(dataB.Iter().Last(), Is.EqualTo(Option<int>.None));
        }

        [Test]
        public void Map()
        {
            int[] dataA = new[] {0, 1, 2, 3};
            Assert.That(
                dataA.Iter().Map(e => e.ToString()).Collect(),
                Is.EquivalentTo(new[] {"0", "1", "2", "3"})
            );
        }

        [Test]
        public void Max()
        {
            Assert.That(Iterator.Empty<int>().Max().IsNone(), Is.True);
            Assert.That(Iterator.Range(0, 10).Max(), Is.EqualTo(Option<int>.Some(9)));
            Assert.That(new int[] {2, 5, 2, 7, 45, 3}.Iter().Max(), Is.EqualTo(Option<int>.Some(45)));
        }

        [Test]
        public void Min()
        {
            Assert.That(Iterator.Empty<int>().Min().IsNone(), Is.True);
            Assert.That(Iterator.Range(0, 10).Min(), Is.EqualTo(Option<int>.Some(0)));
            Assert.That(new int[] {2, 5, 2, 7, 45, 3}.Iter().Min(), Is.EqualTo(Option<int>.Some(2)));
        }

        [Test]
        public void Eq()
        {
            Assert.That(Iterator.Range(0, 5).Eq(new int[] {0, 1, 2, 3, 4}.Iter()), Is.True);
            Assert.That(Iterator.Empty<int>().Eq(new int[0].Iter()), Is.True);
            Assert.That(Iterator.Range(0, 5).Eq(new int[] {1, 2, 3, 4, 0}.Iter()), Is.False);
            Assert.That(Iterator.Range(0, 5).Eq(new int[] {0, 1, 2, 3, 4, 5}.Iter()), Is.False);
            Assert.That(Iterator.Range(0, 6).Eq(new int[] {0, 1, 2, 3, 4}.Iter()), Is.False);
        }

        [Test]
        public void Ne()
        {
            Assert.That(Iterator.Range(0, 5).Ne(new int[] {0, 1, 2, 3, 4}.Iter()), Is.False);
            Assert.That(Iterator.Empty<int>().Ne(new int[0].Iter()), Is.False);
            Assert.That(Iterator.Range(0, 5).Ne(new int[] {1, 2, 3, 4, 0}.Iter()), Is.True);
            Assert.That(Iterator.Range(0, 5).Ne(new int[] {0, 1, 2, 3, 4, 5}.Iter()), Is.True);
            Assert.That(Iterator.Range(0, 6).Ne(new int[] {0, 1, 2, 3, 4}.Iter()), Is.True);
        }

        [Test]
        public void Lt()
        {
            throw new NotImplementedException();
        }

        [Test]
        public void Le()
        {
            throw new NotImplementedException();
        }

        [Test]
        public void Gt()
        {
            throw new NotImplementedException();
        }

        [Test]
        public void Ge()
        {
            throw new NotImplementedException();
        }

        [Test]
        public void Nth()
        {
            int[] dataA = new[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
            Assert.That(dataA.Iter().Nth(-1), Is.EqualTo(Option<int>.None));
            Assert.That(dataA.Iter().Nth(0), Is.EqualTo(Option<int>.Some(0)));
            Assert.That(dataA.Iter().Nth(5), Is.EqualTo(Option<int>.Some(5)));
            Assert.That(dataA.Iter().Nth(9), Is.EqualTo(Option<int>.Some(9)));
            Assert.That(dataA.Iter().Nth(10), Is.EqualTo(Option<int>.None));
            Assert.That(dataA.Iter().Nth(11), Is.EqualTo(Option<int>.None));

            var dataB = new int[0];
            Assert.That(dataA.Iter().Nth(-1), Is.EqualTo(Option<int>.None));
            Assert.That(dataB.Iter().Nth(0), Is.EqualTo(Option<int>.None));
            Assert.That(dataB.Iter().Nth(1), Is.EqualTo(Option<int>.None));
        }

        [Test]
        public void Product()
        {
            int resA = Iterator.Repeat(1)
                .Take(10)
                .Product();
            Assert.That(resA, Is.EqualTo(1));

            uint resB = Iterator.Repeat(1u)
                .Take(10)
                .Product();
            Assert.That(resB, Is.EqualTo(1u));

            long resC = Iterator.Repeat(1L)
                .Take(10)
                .Product();
            Assert.That(resC, Is.EqualTo(1L));

            ulong resD = Iterator.Repeat(1ul)
                .Take(10)
                .Product();
            Assert.That(resD, Is.EqualTo(1ul));

            float resE = Iterator.Repeat(1f)
                .Take(10)
                .Product();
            Assert.That(resE, Is.EqualTo(1f));

            double resF = Iterator.Repeat(1d)
                .Take(10)
                .Product();
            Assert.That(resF, Is.EqualTo(1d));

            decimal resG = Iterator.Repeat(1m)
                .Take(10)
                .Product();
            Assert.That(resG, Is.EqualTo(1m));
        }

        [Test]
        public void Sum()
        {
            int resA = Iterator.Repeat(1)
                .Take(10)
                .Sum();
            Assert.That(resA, Is.EqualTo(10));

            uint resB = Iterator.Repeat(1u)
                .Take(10)
                .Sum();
            Assert.That(resB, Is.EqualTo(10u));

            long resC = Iterator.Repeat(1L)
                .Take(10)
                .Sum();
            Assert.That(resC, Is.EqualTo(10L));

            ulong resD = Iterator.Repeat(1ul)
                .Take(10)
                .Sum();
            Assert.That(resD, Is.EqualTo(10ul));

            float resE = Iterator.Repeat(1f)
                .Take(10)
                .Sum();
            Assert.That(resE, Is.EqualTo(10f));

            double resF = Iterator.Repeat(1d)
                .Take(10)
                .Sum();
            Assert.That(resF, Is.EqualTo(10d));

            decimal resG = Iterator.Repeat(1m)
                .Take(10)
                .Sum();
            Assert.That(resG, Is.EqualTo(10m));
        }

        [Test]
        public void Zip()
        {
            int[] dataA = new[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
            int[] dataB = new[] {10, 11, 12, 13, 14};
            var dataC = new int[0];

            Assert.That(
                dataA.Iter().Zip(dataB.Iter()).Collect(),
                Is.EquivalentTo(new[] {(0, 10), (1, 11), (2, 12), (3, 13), (4, 14)})
            );
            Assert.That(
                dataA.Iter().Zip(dataC.Iter()).Collect(),
                Is.EquivalentTo(new (int, int)[0])
            );
        }
    }
}
