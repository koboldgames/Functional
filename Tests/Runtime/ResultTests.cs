using Koboldgames.Option;
using Koboldgames.Result;
using NUnit.Framework;

namespace Koboldgames.Tests
{
    [TestFixture]
    public class ResultTests
    {
        [Test]
        public void And()
        {
            Result<char, string> resA = Result<int, string>.Ok(100)
                .And(Result<char, string>.Ok('.'));
            Assert.IsTrue(resA.Unwrap() == '.');

            Result<char, string> resB = Result<int, string>.Ok(100)
                .And(Result<char, string>.Err("Second"));
            Assert.IsTrue(resB.UnwrapErr() == "Second");

            Result<char, string> resC = Result<int, string>.Err("First")
                .And(Result<char, string>.Ok('.'));
            Assert.IsTrue(resC.UnwrapErr() == "First");

            Result<char, string> unused = Result<int, string>.Err("First")
                .And(Result<char, string>.Err("Second"));
            Assert.IsTrue(resC.UnwrapErr() == "First");
        }

        [Test]
        public void AndThen()
        {
            Result<char, string> resA = Result<int, string>.Ok(100)
                .AndThen(o => Result<char, string>.Ok('.'));
            Assert.IsTrue(resA.Unwrap() == '.');

            Result<char, string> resB = Result<int, string>.Ok(100)
                .AndThen(o => Result<char, string>.Err("Second"));
            Assert.IsTrue(resB.UnwrapErr() == "Second");

            Result<char, string> resC = Result<int, string>.Err("First")
                .AndThen(o => Result<char, string>.Ok('.'));
            Assert.IsTrue(resC.UnwrapErr() == "First");

            Result<char, string> unused = Result<int, string>.Err("First")
                .AndThen(o => Result<char, string>.Err("Second"));
            Assert.IsTrue(resC.UnwrapErr() == "First");
        }

        [Test]
        public void Err()
        {
            Option<string> optA = Result<int, string>.Ok(100)
                .Err();
            Assert.That(optA, Is.EqualTo(Option<string>.None));

            Option<string> optB = Result<int, string>.Err(string.Empty)
                .Err();
            Assert.That(optB, Is.EqualTo(Option<string>.Some(string.Empty)));
        }

        [Test]
        public void Expect()
        {
            var resA = Result<int, string>.Ok(100);
            Assert.That(resA.Expect(string.Empty), Is.EqualTo(100));

            var resB = Result<int, string>.Err(string.Empty);
            Assert.That(() => resB.Expect(string.Empty), Throws.TypeOf<Result.UnwrapException>());
        }

        [Test]
        public void ExpectErr()
        {
            var resA = Result<int, string>.Ok(100);
            Assert.That(() => resA.ExpectErr(string.Empty), Throws.TypeOf<Result.UnwrapException>());

            var resB = Result<int, string>.Err(string.Empty);
            Assert.That(resB.ExpectErr(string.Empty), Is.EqualTo(string.Empty));
        }

        [Test]
        public void IsErr()
        {
            var resA = Result<int, string>.Ok(100);
            Assert.That(resA.IsErr(), Is.False);

            var resB = Result<int, string>.Err(string.Empty);
            Assert.That(resB.IsErr(), Is.True);
        }

        [Test]
        public void IsOk()
        {
            var resA = Result<int, string>.Ok(100);
            Assert.That(resA.IsOk(), Is.True);

            var resB = Result<int, string>.Err(string.Empty);
            Assert.That(resB.IsOk(), Is.False);
        }

        [Test]
        public void Map()
        {
            Result<char, string> resA = Result<int, string>.Ok(100)
                .Map(o => '.');
            Assert.IsTrue(resA.Unwrap() == '.');

            Result<char, string> resB = Result<int, string>.Err("First")
                .Map(o => '.');
            Assert.IsTrue(resB.IsErr());
        }

        [Test]
        public void MapOr()
        {
            char resA = Result<int, string>.Ok(100)
                .MapOr('_', o => '.');
            Assert.IsTrue(resA == '.');

            char resB = Result<int, string>.Err("First")
                .MapOr('_', o => '.');
            Assert.IsTrue(resB == '_');
        }

        [Test]
        public void MapOrElse()
        {
            char resA = Result<int, string>.Ok(100)
                .MapOrElse(e => '_', o => '.');
            Assert.IsTrue(resA == '.');

            char resB = Result<int, string>.Err("First")
                .MapOrElse(e => '_', o => '.');
            Assert.IsTrue(resB == '_');
        }

        [Test]
        public void Ok()
        {
            Option<int> optA = Result<int, string>.Ok(100)
                .Ok();
            Assert.That(optA, Is.EqualTo(Option<int>.Some(100)));

            Option<int> optB = Result<int, string>.Err(string.Empty)
                .Ok();
            Assert.That(optB, Is.EqualTo(Option<int>.None));
        }

        [Test]
        public void Or()
        {
            Result<int, int> resA = Result<int, string>.Ok(100)
                .Or(Result<int, int>.Ok(200));
            Assert.IsTrue(resA.Unwrap() == 100);

            Result<int, int> resB = Result<int, string>.Ok(100)
                .Or(Result<int, int>.Err(-2000));
            Assert.IsTrue(resB.Unwrap() == 100);

            Result<int, int> resC = Result<int, string>.Err("First")
                .Or(Result<int, int>.Ok(200));
            Assert.IsTrue(resC.Unwrap() == 200);

            Result<int, int> resD = Result<int, string>.Err("First")
                .Or(Result<int, int>.Err(-2000));
            Assert.IsTrue(resD.UnwrapErr() == -2000);
        }

        [Test]
        public void OrElse()
        {
            Result<int, int> resA = Result<int, string>.Ok(100)
                .OrElse(e => Result<int, int>.Ok(200));
            Assert.IsTrue(resA.Unwrap() == 100);

            Result<int, int> resB = Result<int, string>.Ok(100)
                .OrElse(e => Result<int, int>.Err(-2000));
            Assert.IsTrue(resB.Unwrap() == 100);

            Result<int, int> resC = Result<int, string>.Err("First")
                .OrElse(e => Result<int, int>.Ok(200));
            Assert.IsTrue(resC.Unwrap() == 200);

            Result<int, int> resD = Result<int, string>.Err("First")
                .OrElse(e => Result<int, int>.Err(-2000));
            Assert.IsTrue(resD.UnwrapErr() == -2000);
        }

        [Test]
        public void State()
        {
            var resA = Result<int, string>.Ok(100);
            Assert.That(resA.State(), Is.EqualTo(Result.State.Ok));

            var resB = Result<int, string>.Err(string.Empty);
            Assert.That(resB.State(), Is.EqualTo(Result.State.Err));
        }

        [Test]
        public void Unwrap()
        {
            var resA = Result<int, string>.Ok(100);
            Assert.That(resA.Unwrap(), Is.EqualTo(100));

            var resB = Result<int, string>.Err(string.Empty);
            Assert.That(resB.Unwrap, Throws.TypeOf<Result.UnwrapException>());
        }

        [Test]
        public void UnwrapErr()
        {
            var resA = Result<int, string>.Ok(100);
            Assert.That(resA.UnwrapErr, Throws.TypeOf<Result.UnwrapException>());

            var resB = Result<int, string>.Err(string.Empty);
            Assert.That(resB.UnwrapErr(), Is.EqualTo(string.Empty));
        }
    }
}
