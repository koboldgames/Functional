# Koboldgames Functional

A collection of functional programming concepts

See https://gitlab.com/koboldgames/Functional.git

## Contents

- `Option`: An option/maybe type for representing optional presence of a value.
- `Result`: A type to represent a calculation result that may have failed with an error.

